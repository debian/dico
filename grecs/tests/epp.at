# This file is part of grecs  -*- Autotest -*-
# Copyright (C) 2021-2022 Sergey Poznyakoff
#
# Grecs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Grecs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Grecs.  If not, see <http://www.gnu.org/licenses/>.
AT_SETUP([External preprocessor])

AT_CHECK([m4 -s </dev/null 2>/dev/null || AT_SKIP_TEST],[0],[stdout])
# On FreeBSD, m4 outputs a single '#line 1 "stdin"' line
AT_CHECK([s=`sed -e '/^#line/d' stdout`; test -z "$s" || AT_SKIP_TEST])

AT_CHECK([
AT_DATA([input],
[define(`X',`cupidatat non proident, sunt in culpa
qui officia deserunt mollit')dnl
Duis aute irure dolor X anim id est laborum.
])
gcfpp -pm4 input
],
[0],
[#line 1 "input"
Duis aute irure dolor cupidatat non proident, sunt in culpa
qui officia deserunt mollit anim id est laborum.
])

AT_CHECK([
AT_DATA([input],
[define(`FWD',`forward {
   address "$1";
   dist $2;
}')dnl
id 1;
FWD(foo, 10)
test "end";
])
gcffmt -preproc=m4 input
],
[0],
[.id: "1"
.forward.address: "foo"
.forward.dist: "10"
.test: "end"
])

#
# Check the pp-setup file
#
AT_CHECK([
mkdir inc
AT_DATA([inc/pp-setup],
[define(`FWD',`forward {
   address "$1";
   dist $2;
}')
])

AT_DATA([input],
[foo X;
FWD(bar,15)
])

gcffmt -preproc=m4 -I$(pwd)/inc input
],
[0],
[.foo: "X"
.forward.address: "bar"
.forward.dist: "15"
])

AT_CLEANUP
