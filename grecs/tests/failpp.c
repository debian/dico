#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <ctype.h>

static char errmark[] = "stop+";
#define ERRLEN (sizeof(errmark)-1)

int
main(int argc, char **argv)
{
	int c;
	int i = 0;
	int bol = 1;
	int ex = 0;
	FILE *input;
	
	if (argc > 1) {
		input = fopen(argv[1], "r");
		if (!input) {
			perror("input");
			return 1;
		}
	}

	while ((c = fgetc(input)) != EOF) {
		if (bol) {
			if (i < ERRLEN) {
				if (c != errmark[i]) {
					if (i)
						fwrite(errmark, i, 1, stdout);
					fputc(c, stdout);
					bol = i = 0;
				}
			} else if (i == ERRLEN) {
				if (!isdigit(c)) {
					fwrite(errmark, i, 1, stdout);
					fputc(c, stdout);
					bol = i = 0;
				} 
				ex = c - '0';
			} else if (isdigit(c)) {
				ex = ex * 10 + c - '0';
			} else {
				return ex;
			}
			i++;
		} else {
			fputc(c, stdout);
			if (c == '\n') {
				bol = 1;
				i = 0;
			}
		}
	}
	return 0;
}
	
	
