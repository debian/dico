/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or grecs_dhcpd_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with grecs_dhcpd or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "dhcpd-gram.y"

/* grecs - Gray's Extensible Configuration System
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Grecs is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Grecs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with Grecs. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <grecs.h>
#include <dhcpd-gram.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

int grecs_dhcpdlex(void);
int grecs_dhcpderror(char const *s);

static struct grecs_node *parse_tree;
extern int grecs_dhcpd_flex_debug;
extern int grecs_dhcpd_new_source(const char *name, grecs_locus_t *loc);
extern void grecs_dhcpd_close_sources(void);

extern void grecs_dhcpd_begin_bool(void);
extern void grecs_dhcpd_begin_expr(void);

/* NOTE: STRING must be allocated */
static grecs_value_t *
make_string_value(char *string)
{
	grecs_value_t *val;
	val = grecs_malloc(sizeof(val[0]));
	val->type = GRECS_TYPE_STRING;
	val->v.string = string;
	return val;
}


#line 122 "dhcpd-gram.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_DHCPD_GRAM_H_INCLUDED
# define YY_YY_DHCPD_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int grecs_dhcpddebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum grecs_dhcpdtokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    DHCPD_IF = 258,                /* DHCPD_IF  */
    DHCPD_ELSIF = 259,             /* DHCPD_ELSIF  */
    DHCPD_EXPR = 260,              /* DHCPD_EXPR  */
    DHCPD_ELSE = 261,              /* DHCPD_ELSE  */
    DHCPD_STRING = 262,            /* DHCPD_STRING  */
    DHCPD_IDENT = 263              /* DHCPD_IDENT  */
  };
  typedef enum grecs_dhcpdtokentype grecs_dhcpdtoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define DHCPD_IF 258
#define DHCPD_ELSIF 259
#define DHCPD_EXPR 260
#define DHCPD_ELSE 261
#define DHCPD_STRING 262
#define DHCPD_IDENT 263

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 55 "dhcpd-gram.y"

	char *string;
	grecs_value_t svalue, *pvalue;
	struct grecs_list *list;
	struct grecs_node *node;
	grecs_locus_t locus;
	struct { struct grecs_node *head, *tail; } node_list;

#line 200 "dhcpd-gram.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE grecs_dhcpdlval;
extern YYLTYPE grecs_dhcpdlloc;

int grecs_dhcpdparse (void);


#endif /* !YY_YY_DHCPD_GRAM_H_INCLUDED  */
/* Symbol kind.  */
enum grecs_dhcpdsymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_DHCPD_IF = 3,                   /* DHCPD_IF  */
  YYSYMBOL_DHCPD_ELSIF = 4,                /* DHCPD_ELSIF  */
  YYSYMBOL_DHCPD_EXPR = 5,                 /* DHCPD_EXPR  */
  YYSYMBOL_DHCPD_ELSE = 6,                 /* DHCPD_ELSE  */
  YYSYMBOL_DHCPD_STRING = 7,               /* DHCPD_STRING  */
  YYSYMBOL_DHCPD_IDENT = 8,                /* DHCPD_IDENT  */
  YYSYMBOL_9_ = 9,                         /* ';'  */
  YYSYMBOL_10_ = 10,                       /* '='  */
  YYSYMBOL_11_ = 11,                       /* '{'  */
  YYSYMBOL_12_ = 12,                       /* '}'  */
  YYSYMBOL_13_ = 13,                       /* ','  */
  YYSYMBOL_YYACCEPT = 14,                  /* $accept  */
  YYSYMBOL_input = 15,                     /* input  */
  YYSYMBOL_maybe_stmtlist = 16,            /* maybe_stmtlist  */
  YYSYMBOL_stmtlist = 17,                  /* stmtlist  */
  YYSYMBOL_stmt = 18,                      /* stmt  */
  YYSYMBOL_simple = 19,                    /* simple  */
  YYSYMBOL_20_1 = 20,                      /* $@1  */
  YYSYMBOL_block = 21,                     /* block  */
  YYSYMBOL_opt_semi = 22,                  /* opt_semi  */
  YYSYMBOL_tag = 23,                       /* tag  */
  YYSYMBOL_vallist = 24,                   /* vallist  */
  YYSYMBOL_vlist = 25,                     /* vlist  */
  YYSYMBOL_value = 26,                     /* value  */
  YYSYMBOL_string = 27,                    /* string  */
  YYSYMBOL_strlist = 28,                   /* strlist  */
  YYSYMBOL_cond = 29,                      /* cond  */
  YYSYMBOL_if = 30,                        /* if  */
  YYSYMBOL_elsif = 31,                     /* elsif  */
  YYSYMBOL_opt_elsifchain = 32,            /* opt_elsifchain  */
  YYSYMBOL_elsifchain = 33,                /* elsifchain  */
  YYSYMBOL_elsifcond = 34,                 /* elsifcond  */
  YYSYMBOL_opt_elsecond = 35,              /* opt_elsecond  */
  YYSYMBOL_elsecond = 36                   /* elsecond  */
};
typedef enum grecs_dhcpdsymbol_kind_t grecs_dhcpdsymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ grecs_dhcpdtype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t grecs_dhcpdtype_int8;
#else
typedef signed char grecs_dhcpdtype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ grecs_dhcpdtype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t grecs_dhcpdtype_int16;
#else
typedef short grecs_dhcpdtype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ grecs_dhcpdtype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t grecs_dhcpdtype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char grecs_dhcpdtype_uint8;
#else
typedef short grecs_dhcpdtype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ grecs_dhcpdtype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t grecs_dhcpdtype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short grecs_dhcpdtype_uint16;
#else
typedef int grecs_dhcpdtype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef grecs_dhcpdtype_int8 grecs_dhcpd_state_t;

/* State numbers in computations.  */
typedef int grecs_dhcpd_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about grecs_dhcpdlval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined grecs_dhcpdoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union grecs_dhcpdalloc
{
  grecs_dhcpd_state_t grecs_dhcpdss_alloc;
  YYSTYPE grecs_dhcpdvs_alloc;
  YYLTYPE grecs_dhcpdls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union grecs_dhcpdalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (grecs_dhcpd_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T grecs_dhcpdnewbytes;                                         \
        YYCOPY (&grecs_dhcpdptr->Stack_alloc, Stack, grecs_dhcpdsize);                    \
        Stack = &grecs_dhcpdptr->Stack_alloc;                                    \
        grecs_dhcpdnewbytes = grecs_dhcpdstacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        grecs_dhcpdptr += grecs_dhcpdnewbytes / YYSIZEOF (*grecs_dhcpdptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T grecs_dhcpdi;                      \
          for (grecs_dhcpdi = 0; grecs_dhcpdi < (Count); grecs_dhcpdi++)   \
            (Dst)[grecs_dhcpdi] = (Src)[grecs_dhcpdi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  22
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   39

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  14
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  23
/* YYNRULES -- Number of rules.  */
#define YYNRULES  38
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  59

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   263


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_dhcpdlex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (grecs_dhcpdsymbol_kind_t, grecs_dhcpdtranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_dhcpdlex.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdtranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    13,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     9,
       2,    10,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    11,     2,    12,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const grecs_dhcpdtype_int16 grecs_dhcpdrline[] =
{
       0,    77,    77,    87,    90,    96,   100,   112,   113,   114,
     117,   131,   131,   139,   148,   159,   160,   164,   167,   170,
     194,   199,   205,   211,   219,   220,   223,   239,   250,   271,
     277,   284,   287,   290,   294,   306,   322,   325,   328
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (grecs_dhcpdsymbol_kind_t, grecs_dhcpdstos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *grecs_dhcpdsymbol_name (grecs_dhcpdsymbol_kind_t grecs_dhcpdsymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const grecs_dhcpdtname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "DHCPD_IF",
  "DHCPD_ELSIF", "DHCPD_EXPR", "DHCPD_ELSE", "DHCPD_STRING", "DHCPD_IDENT",
  "';'", "'='", "'{'", "'}'", "','", "$accept", "input", "maybe_stmtlist",
  "stmtlist", "stmt", "simple", "$@1", "block", "opt_semi", "tag",
  "vallist", "vlist", "value", "string", "strlist", "cond", "if", "elsif",
  "opt_elsifchain", "elsifchain", "elsifcond", "opt_elsecond", "elsecond", YY_NULLPTR
};

static const char *
grecs_dhcpdsymbol_name (grecs_dhcpdsymbol_kind_t grecs_dhcpdsymbol)
{
  return grecs_dhcpdtname[grecs_dhcpdsymbol];
}
#endif

#define YYPACT_NINF (-29)

#define grecs_dhcpdpact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-26)

#define grecs_dhcpdtable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdpact[] =
{
      -1,   -29,   -29,     1,     3,   -29,    -1,   -29,   -29,   -29,
      -4,   -29,     9,     2,   -29,   -29,     6,    10,     5,   -29,
     -29,     7,   -29,   -29,   -29,    11,    14,    13,    -1,   -29,
     -29,    16,    -1,   -29,    15,    17,   -29,    18,   -29,    19,
      23,   -29,   -29,   -29,    26,    27,    23,   -29,    21,    24,
     -29,   -29,   -29,    -1,    -1,    22,    25,   -29,   -29
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const grecs_dhcpdtype_int8 grecs_dhcpddefact[] =
{
       3,    29,    24,    17,     0,     2,     4,     5,     7,     8,
       0,     9,     0,    24,    25,    11,     0,    18,    19,    20,
      22,    23,     1,     6,    13,     0,     0,     0,     3,    10,
      21,     0,     3,    26,     0,     0,    27,     0,    12,    15,
      31,    16,    14,    30,     0,    36,    32,    33,     0,     0,
      28,    37,    34,     3,     3,     0,     0,    35,    38
};

/* YYPGOTO[NTERM-NUM].  */
static const grecs_dhcpdtype_int8 grecs_dhcpdpgoto[] =
{
     -29,   -29,   -28,   -29,    30,   -29,   -29,   -29,   -29,   -29,
     -29,   -29,    20,    -2,   -29,   -29,   -29,   -29,   -29,   -29,
      -7,   -29,   -29
};

/* YYDEFGOTO[NTERM-NUM].  */
static const grecs_dhcpdtype_int8 grecs_dhcpddefgoto[] =
{
       0,     4,     5,     6,     7,     8,    27,     9,    42,    16,
      17,    18,    19,    10,    21,    11,    12,    44,    45,    46,
      47,    50,    51
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdtable[] =
{
      35,    20,     1,    22,    37,    24,     2,     3,    13,    14,
     -25,    15,    13,    14,    25,    26,    20,    28,    34,    29,
      31,    33,    32,    36,    38,    55,    56,    43,    41,    39,
      40,    48,    53,    49,    57,    54,    23,    58,    30,    52
};

static const grecs_dhcpdtype_int8 grecs_dhcpdcheck[] =
{
      28,     3,     3,     0,    32,     9,     7,     8,     7,     8,
       9,    10,     7,     8,     5,    13,    18,    11,     5,     9,
      13,     7,    11,     7,     9,    53,    54,     4,     9,    12,
      12,     5,    11,     6,    12,    11,     6,    12,    18,    46
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdstos[] =
{
       0,     3,     7,     8,    15,    16,    17,    18,    19,    21,
      27,    29,    30,     7,     8,    10,    23,    24,    25,    26,
      27,    28,     0,    18,     9,     5,    13,    20,    11,     9,
      26,    13,    11,     7,     5,    16,     7,    16,     9,    12,
      12,     9,    22,     4,    31,    32,    33,    34,     5,     6,
      35,    36,    34,    11,    11,    16,    16,    12,    12
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdr1[] =
{
       0,    14,    15,    16,    16,    17,    17,    18,    18,    18,
      19,    20,    19,    19,    21,    22,    22,    23,    23,    24,
      25,    25,    26,    26,    27,    27,    28,    28,    29,    30,
      31,    32,    32,    33,    33,    34,    35,    35,    36
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const grecs_dhcpdtype_int8 grecs_dhcpdr2[] =
{
       0,     2,     1,     0,     1,     1,     2,     1,     1,     1,
       3,     0,     5,     2,     6,     0,     1,     0,     1,     1,
       1,     2,     1,     1,     1,     1,     3,     3,     7,     1,
       1,     0,     1,     1,     2,     5,     0,     1,     4
};


enum { YYENOMEM = -2 };

#define grecs_dhcpderrok         (grecs_dhcpderrstatus = 0)
#define grecs_dhcpdclearin       (grecs_dhcpdchar = YYEMPTY)

#define YYACCEPT        goto grecs_dhcpdacceptlab
#define YYABORT         goto grecs_dhcpdabortlab
#define YYERROR         goto grecs_dhcpderrorlab
#define YYNOMEM         goto grecs_dhcpdexhaustedlab


#define YYRECOVERING()  (!!grecs_dhcpderrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (grecs_dhcpdchar == YYEMPTY)                                        \
      {                                                           \
        grecs_dhcpdchar = (Token);                                         \
        grecs_dhcpdlval = (Value);                                         \
        YYPOPSTACK (grecs_dhcpdlen);                                       \
        grecs_dhcpdstate = *grecs_dhcpdssp;                                         \
        goto grecs_dhcpdbackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        grecs_dhcpderror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (grecs_dhcpddebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
grecs_dhcpd_location_print_ (FILE *grecs_dhcpdo, YYLTYPE const * const grecs_dhcpdlocp)
{
  int res = 0;
  int end_col = 0 != grecs_dhcpdlocp->last_column ? grecs_dhcpdlocp->last_column - 1 : 0;
  if (0 <= grecs_dhcpdlocp->first_line)
    {
      res += YYFPRINTF (grecs_dhcpdo, "%d", grecs_dhcpdlocp->first_line);
      if (0 <= grecs_dhcpdlocp->first_column)
        res += YYFPRINTF (grecs_dhcpdo, ".%d", grecs_dhcpdlocp->first_column);
    }
  if (0 <= grecs_dhcpdlocp->last_line)
    {
      if (grecs_dhcpdlocp->first_line < grecs_dhcpdlocp->last_line)
        {
          res += YYFPRINTF (grecs_dhcpdo, "-%d", grecs_dhcpdlocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (grecs_dhcpdo, ".%d", end_col);
        }
      else if (0 <= end_col && grecs_dhcpdlocp->first_column < end_col)
        res += YYFPRINTF (grecs_dhcpdo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  grecs_dhcpd_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (grecs_dhcpddebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      grecs_dhcpd_symbol_print (stderr,                                            \
                  Kind, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
grecs_dhcpd_symbol_value_print (FILE *grecs_dhcpdo,
                       grecs_dhcpdsymbol_kind_t grecs_dhcpdkind, YYSTYPE const * const grecs_dhcpdvaluep, YYLTYPE const * const grecs_dhcpdlocationp)
{
  FILE *grecs_dhcpdoutput = grecs_dhcpdo;
  YY_USE (grecs_dhcpdoutput);
  YY_USE (grecs_dhcpdlocationp);
  if (!grecs_dhcpdvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_dhcpdkind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
grecs_dhcpd_symbol_print (FILE *grecs_dhcpdo,
                 grecs_dhcpdsymbol_kind_t grecs_dhcpdkind, YYSTYPE const * const grecs_dhcpdvaluep, YYLTYPE const * const grecs_dhcpdlocationp)
{
  YYFPRINTF (grecs_dhcpdo, "%s %s (",
             grecs_dhcpdkind < YYNTOKENS ? "token" : "nterm", grecs_dhcpdsymbol_name (grecs_dhcpdkind));

  YYLOCATION_PRINT (grecs_dhcpdo, grecs_dhcpdlocationp);
  YYFPRINTF (grecs_dhcpdo, ": ");
  grecs_dhcpd_symbol_value_print (grecs_dhcpdo, grecs_dhcpdkind, grecs_dhcpdvaluep, grecs_dhcpdlocationp);
  YYFPRINTF (grecs_dhcpdo, ")");
}

/*------------------------------------------------------------------.
| grecs_dhcpd_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
grecs_dhcpd_stack_print (grecs_dhcpd_state_t *grecs_dhcpdbottom, grecs_dhcpd_state_t *grecs_dhcpdtop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; grecs_dhcpdbottom <= grecs_dhcpdtop; grecs_dhcpdbottom++)
    {
      int grecs_dhcpdbot = *grecs_dhcpdbottom;
      YYFPRINTF (stderr, " %d", grecs_dhcpdbot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (grecs_dhcpddebug)                                                  \
    grecs_dhcpd_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
grecs_dhcpd_reduce_print (grecs_dhcpd_state_t *grecs_dhcpdssp, YYSTYPE *grecs_dhcpdvsp, YYLTYPE *grecs_dhcpdlsp,
                 int grecs_dhcpdrule)
{
  int grecs_dhcpdlno = grecs_dhcpdrline[grecs_dhcpdrule];
  int grecs_dhcpdnrhs = grecs_dhcpdr2[grecs_dhcpdrule];
  int grecs_dhcpdi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             grecs_dhcpdrule - 1, grecs_dhcpdlno);
  /* The symbols being reduced.  */
  for (grecs_dhcpdi = 0; grecs_dhcpdi < grecs_dhcpdnrhs; grecs_dhcpdi++)
    {
      YYFPRINTF (stderr, "   $%d = ", grecs_dhcpdi + 1);
      grecs_dhcpd_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+grecs_dhcpdssp[grecs_dhcpdi + 1 - grecs_dhcpdnrhs]),
                       &grecs_dhcpdvsp[(grecs_dhcpdi + 1) - (grecs_dhcpdnrhs)],
                       &(grecs_dhcpdlsp[(grecs_dhcpdi + 1) - (grecs_dhcpdnrhs)]));
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (grecs_dhcpddebug)                          \
    grecs_dhcpd_reduce_print (grecs_dhcpdssp, grecs_dhcpdvsp, grecs_dhcpdlsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int grecs_dhcpddebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  grecs_dhcpd_state_t *grecs_dhcpdssp;
  grecs_dhcpdsymbol_kind_t grecs_dhcpdtoken;
  YYLTYPE *grecs_dhcpdlloc;
} grecs_dhcpdpcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
grecs_dhcpdpcontext_expected_tokens (const grecs_dhcpdpcontext_t *grecs_dhcpdctx,
                            grecs_dhcpdsymbol_kind_t grecs_dhcpdarg[], int grecs_dhcpdargn)
{
  /* Actual size of YYARG. */
  int grecs_dhcpdcount = 0;
  int grecs_dhcpdn = grecs_dhcpdpact[+*grecs_dhcpdctx->grecs_dhcpdssp];
  if (!grecs_dhcpdpact_value_is_default (grecs_dhcpdn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int grecs_dhcpdxbegin = grecs_dhcpdn < 0 ? -grecs_dhcpdn : 0;
      /* Stay within bounds of both grecs_dhcpdcheck and grecs_dhcpdtname.  */
      int grecs_dhcpdchecklim = YYLAST - grecs_dhcpdn + 1;
      int grecs_dhcpdxend = grecs_dhcpdchecklim < YYNTOKENS ? grecs_dhcpdchecklim : YYNTOKENS;
      int grecs_dhcpdx;
      for (grecs_dhcpdx = grecs_dhcpdxbegin; grecs_dhcpdx < grecs_dhcpdxend; ++grecs_dhcpdx)
        if (grecs_dhcpdcheck[grecs_dhcpdx + grecs_dhcpdn] == grecs_dhcpdx && grecs_dhcpdx != YYSYMBOL_YYerror
            && !grecs_dhcpdtable_value_is_error (grecs_dhcpdtable[grecs_dhcpdx + grecs_dhcpdn]))
          {
            if (!grecs_dhcpdarg)
              ++grecs_dhcpdcount;
            else if (grecs_dhcpdcount == grecs_dhcpdargn)
              return 0;
            else
              grecs_dhcpdarg[grecs_dhcpdcount++] = YY_CAST (grecs_dhcpdsymbol_kind_t, grecs_dhcpdx);
          }
    }
  if (grecs_dhcpdarg && grecs_dhcpdcount == 0 && 0 < grecs_dhcpdargn)
    grecs_dhcpdarg[0] = YYSYMBOL_YYEMPTY;
  return grecs_dhcpdcount;
}




#ifndef grecs_dhcpdstrlen
# if defined __GLIBC__ && defined _STRING_H
#  define grecs_dhcpdstrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
grecs_dhcpdstrlen (const char *grecs_dhcpdstr)
{
  YYPTRDIFF_T grecs_dhcpdlen;
  for (grecs_dhcpdlen = 0; grecs_dhcpdstr[grecs_dhcpdlen]; grecs_dhcpdlen++)
    continue;
  return grecs_dhcpdlen;
}
# endif
#endif

#ifndef grecs_dhcpdstpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define grecs_dhcpdstpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
grecs_dhcpdstpcpy (char *grecs_dhcpddest, const char *grecs_dhcpdsrc)
{
  char *grecs_dhcpdd = grecs_dhcpddest;
  const char *grecs_dhcpds = grecs_dhcpdsrc;

  while ((*grecs_dhcpdd++ = *grecs_dhcpds++) != '\0')
    continue;

  return grecs_dhcpdd - 1;
}
# endif
#endif

#ifndef grecs_dhcpdtnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for grecs_dhcpderror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from grecs_dhcpdtname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
grecs_dhcpdtnamerr (char *grecs_dhcpdres, const char *grecs_dhcpdstr)
{
  if (*grecs_dhcpdstr == '"')
    {
      YYPTRDIFF_T grecs_dhcpdn = 0;
      char const *grecs_dhcpdp = grecs_dhcpdstr;
      for (;;)
        switch (*++grecs_dhcpdp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++grecs_dhcpdp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (grecs_dhcpdres)
              grecs_dhcpdres[grecs_dhcpdn] = *grecs_dhcpdp;
            grecs_dhcpdn++;
            break;

          case '"':
            if (grecs_dhcpdres)
              grecs_dhcpdres[grecs_dhcpdn] = '\0';
            return grecs_dhcpdn;
          }
    do_not_strip_quotes: ;
    }

  if (grecs_dhcpdres)
    return grecs_dhcpdstpcpy (grecs_dhcpdres, grecs_dhcpdstr) - grecs_dhcpdres;
  else
    return grecs_dhcpdstrlen (grecs_dhcpdstr);
}
#endif


static int
grecs_dhcpd_syntax_error_arguments (const grecs_dhcpdpcontext_t *grecs_dhcpdctx,
                           grecs_dhcpdsymbol_kind_t grecs_dhcpdarg[], int grecs_dhcpdargn)
{
  /* Actual size of YYARG. */
  int grecs_dhcpdcount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in grecs_dhcpdchar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated grecs_dhcpdchar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (grecs_dhcpdctx->grecs_dhcpdtoken != YYSYMBOL_YYEMPTY)
    {
      int grecs_dhcpdn;
      if (grecs_dhcpdarg)
        grecs_dhcpdarg[grecs_dhcpdcount] = grecs_dhcpdctx->grecs_dhcpdtoken;
      ++grecs_dhcpdcount;
      grecs_dhcpdn = grecs_dhcpdpcontext_expected_tokens (grecs_dhcpdctx,
                                        grecs_dhcpdarg ? grecs_dhcpdarg + 1 : grecs_dhcpdarg, grecs_dhcpdargn - 1);
      if (grecs_dhcpdn == YYENOMEM)
        return YYENOMEM;
      else
        grecs_dhcpdcount += grecs_dhcpdn;
    }
  return grecs_dhcpdcount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
grecs_dhcpdsyntax_error (YYPTRDIFF_T *grecs_dhcpdmsg_alloc, char **grecs_dhcpdmsg,
                const grecs_dhcpdpcontext_t *grecs_dhcpdctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *grecs_dhcpdformat = YY_NULLPTR;
  /* Arguments of grecs_dhcpdformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  grecs_dhcpdsymbol_kind_t grecs_dhcpdarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T grecs_dhcpdsize = 0;

  /* Actual size of YYARG. */
  int grecs_dhcpdcount = grecs_dhcpd_syntax_error_arguments (grecs_dhcpdctx, grecs_dhcpdarg, YYARGS_MAX);
  if (grecs_dhcpdcount == YYENOMEM)
    return YYENOMEM;

  switch (grecs_dhcpdcount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        grecs_dhcpdformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  grecs_dhcpdsize = grecs_dhcpdstrlen (grecs_dhcpdformat) - 2 * grecs_dhcpdcount + 1;
  {
    int grecs_dhcpdi;
    for (grecs_dhcpdi = 0; grecs_dhcpdi < grecs_dhcpdcount; ++grecs_dhcpdi)
      {
        YYPTRDIFF_T grecs_dhcpdsize1
          = grecs_dhcpdsize + grecs_dhcpdtnamerr (YY_NULLPTR, grecs_dhcpdtname[grecs_dhcpdarg[grecs_dhcpdi]]);
        if (grecs_dhcpdsize <= grecs_dhcpdsize1 && grecs_dhcpdsize1 <= YYSTACK_ALLOC_MAXIMUM)
          grecs_dhcpdsize = grecs_dhcpdsize1;
        else
          return YYENOMEM;
      }
  }

  if (*grecs_dhcpdmsg_alloc < grecs_dhcpdsize)
    {
      *grecs_dhcpdmsg_alloc = 2 * grecs_dhcpdsize;
      if (! (grecs_dhcpdsize <= *grecs_dhcpdmsg_alloc
             && *grecs_dhcpdmsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *grecs_dhcpdmsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *grecs_dhcpdp = *grecs_dhcpdmsg;
    int grecs_dhcpdi = 0;
    while ((*grecs_dhcpdp = *grecs_dhcpdformat) != '\0')
      if (*grecs_dhcpdp == '%' && grecs_dhcpdformat[1] == 's' && grecs_dhcpdi < grecs_dhcpdcount)
        {
          grecs_dhcpdp += grecs_dhcpdtnamerr (grecs_dhcpdp, grecs_dhcpdtname[grecs_dhcpdarg[grecs_dhcpdi++]]);
          grecs_dhcpdformat += 2;
        }
      else
        {
          ++grecs_dhcpdp;
          ++grecs_dhcpdformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
grecs_dhcpddestruct (const char *grecs_dhcpdmsg,
            grecs_dhcpdsymbol_kind_t grecs_dhcpdkind, YYSTYPE *grecs_dhcpdvaluep, YYLTYPE *grecs_dhcpdlocationp)
{
  YY_USE (grecs_dhcpdvaluep);
  YY_USE (grecs_dhcpdlocationp);
  if (!grecs_dhcpdmsg)
    grecs_dhcpdmsg = "Deleting";
  YY_SYMBOL_PRINT (grecs_dhcpdmsg, grecs_dhcpdkind, grecs_dhcpdvaluep, grecs_dhcpdlocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_dhcpdkind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int grecs_dhcpdchar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE grecs_dhcpdlval;
/* Location data for the lookahead symbol.  */
YYLTYPE grecs_dhcpdlloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int grecs_dhcpdnerrs;




/*----------.
| grecs_dhcpdparse.  |
`----------*/

int
grecs_dhcpdparse (void)
{
    grecs_dhcpd_state_fast_t grecs_dhcpdstate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int grecs_dhcpderrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow grecs_dhcpdoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T grecs_dhcpdstacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    grecs_dhcpd_state_t grecs_dhcpdssa[YYINITDEPTH];
    grecs_dhcpd_state_t *grecs_dhcpdss = grecs_dhcpdssa;
    grecs_dhcpd_state_t *grecs_dhcpdssp = grecs_dhcpdss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE grecs_dhcpdvsa[YYINITDEPTH];
    YYSTYPE *grecs_dhcpdvs = grecs_dhcpdvsa;
    YYSTYPE *grecs_dhcpdvsp = grecs_dhcpdvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE grecs_dhcpdlsa[YYINITDEPTH];
    YYLTYPE *grecs_dhcpdls = grecs_dhcpdlsa;
    YYLTYPE *grecs_dhcpdlsp = grecs_dhcpdls;

  int grecs_dhcpdn;
  /* The return value of grecs_dhcpdparse.  */
  int grecs_dhcpdresult;
  /* Lookahead symbol kind.  */
  grecs_dhcpdsymbol_kind_t grecs_dhcpdtoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE grecs_dhcpdval;
  YYLTYPE grecs_dhcpdloc;

  /* The locations where the error started and ended.  */
  YYLTYPE grecs_dhcpderror_range[3];

  /* Buffer for error messages, and its allocated size.  */
  char grecs_dhcpdmsgbuf[128];
  char *grecs_dhcpdmsg = grecs_dhcpdmsgbuf;
  YYPTRDIFF_T grecs_dhcpdmsg_alloc = sizeof grecs_dhcpdmsgbuf;

#define YYPOPSTACK(N)   (grecs_dhcpdvsp -= (N), grecs_dhcpdssp -= (N), grecs_dhcpdlsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int grecs_dhcpdlen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  grecs_dhcpdchar = YYEMPTY; /* Cause a token to be read.  */

  grecs_dhcpdlsp[0] = grecs_dhcpdlloc;
  goto grecs_dhcpdsetstate;


/*------------------------------------------------------------.
| grecs_dhcpdnewstate -- push a new state, which is found in grecs_dhcpdstate.  |
`------------------------------------------------------------*/
grecs_dhcpdnewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  grecs_dhcpdssp++;


/*--------------------------------------------------------------------.
| grecs_dhcpdsetstate -- set current state (the top of the stack) to grecs_dhcpdstate.  |
`--------------------------------------------------------------------*/
grecs_dhcpdsetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", grecs_dhcpdstate));
  YY_ASSERT (0 <= grecs_dhcpdstate && grecs_dhcpdstate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *grecs_dhcpdssp = YY_CAST (grecs_dhcpd_state_t, grecs_dhcpdstate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (grecs_dhcpdss, grecs_dhcpdssp);

  if (grecs_dhcpdss + grecs_dhcpdstacksize - 1 <= grecs_dhcpdssp)
#if !defined grecs_dhcpdoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T grecs_dhcpdsize = grecs_dhcpdssp - grecs_dhcpdss + 1;

# if defined grecs_dhcpdoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        grecs_dhcpd_state_t *grecs_dhcpdss1 = grecs_dhcpdss;
        YYSTYPE *grecs_dhcpdvs1 = grecs_dhcpdvs;
        YYLTYPE *grecs_dhcpdls1 = grecs_dhcpdls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if grecs_dhcpdoverflow is a macro.  */
        grecs_dhcpdoverflow (YY_("memory exhausted"),
                    &grecs_dhcpdss1, grecs_dhcpdsize * YYSIZEOF (*grecs_dhcpdssp),
                    &grecs_dhcpdvs1, grecs_dhcpdsize * YYSIZEOF (*grecs_dhcpdvsp),
                    &grecs_dhcpdls1, grecs_dhcpdsize * YYSIZEOF (*grecs_dhcpdlsp),
                    &grecs_dhcpdstacksize);
        grecs_dhcpdss = grecs_dhcpdss1;
        grecs_dhcpdvs = grecs_dhcpdvs1;
        grecs_dhcpdls = grecs_dhcpdls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= grecs_dhcpdstacksize)
        YYNOMEM;
      grecs_dhcpdstacksize *= 2;
      if (YYMAXDEPTH < grecs_dhcpdstacksize)
        grecs_dhcpdstacksize = YYMAXDEPTH;

      {
        grecs_dhcpd_state_t *grecs_dhcpdss1 = grecs_dhcpdss;
        union grecs_dhcpdalloc *grecs_dhcpdptr =
          YY_CAST (union grecs_dhcpdalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (grecs_dhcpdstacksize))));
        if (! grecs_dhcpdptr)
          YYNOMEM;
        YYSTACK_RELOCATE (grecs_dhcpdss_alloc, grecs_dhcpdss);
        YYSTACK_RELOCATE (grecs_dhcpdvs_alloc, grecs_dhcpdvs);
        YYSTACK_RELOCATE (grecs_dhcpdls_alloc, grecs_dhcpdls);
#  undef YYSTACK_RELOCATE
        if (grecs_dhcpdss1 != grecs_dhcpdssa)
          YYSTACK_FREE (grecs_dhcpdss1);
      }
# endif

      grecs_dhcpdssp = grecs_dhcpdss + grecs_dhcpdsize - 1;
      grecs_dhcpdvsp = grecs_dhcpdvs + grecs_dhcpdsize - 1;
      grecs_dhcpdlsp = grecs_dhcpdls + grecs_dhcpdsize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, grecs_dhcpdstacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (grecs_dhcpdss + grecs_dhcpdstacksize - 1 <= grecs_dhcpdssp)
        YYABORT;
    }
#endif /* !defined grecs_dhcpdoverflow && !defined YYSTACK_RELOCATE */


  if (grecs_dhcpdstate == YYFINAL)
    YYACCEPT;

  goto grecs_dhcpdbackup;


/*-----------.
| grecs_dhcpdbackup.  |
`-----------*/
grecs_dhcpdbackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  grecs_dhcpdn = grecs_dhcpdpact[grecs_dhcpdstate];
  if (grecs_dhcpdpact_value_is_default (grecs_dhcpdn))
    goto grecs_dhcpddefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (grecs_dhcpdchar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      grecs_dhcpdchar = grecs_dhcpdlex ();
    }

  if (grecs_dhcpdchar <= YYEOF)
    {
      grecs_dhcpdchar = YYEOF;
      grecs_dhcpdtoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (grecs_dhcpdchar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      grecs_dhcpdchar = YYUNDEF;
      grecs_dhcpdtoken = YYSYMBOL_YYerror;
      grecs_dhcpderror_range[1] = grecs_dhcpdlloc;
      goto grecs_dhcpderrlab1;
    }
  else
    {
      grecs_dhcpdtoken = YYTRANSLATE (grecs_dhcpdchar);
      YY_SYMBOL_PRINT ("Next token is", grecs_dhcpdtoken, &grecs_dhcpdlval, &grecs_dhcpdlloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  grecs_dhcpdn += grecs_dhcpdtoken;
  if (grecs_dhcpdn < 0 || YYLAST < grecs_dhcpdn || grecs_dhcpdcheck[grecs_dhcpdn] != grecs_dhcpdtoken)
    goto grecs_dhcpddefault;
  grecs_dhcpdn = grecs_dhcpdtable[grecs_dhcpdn];
  if (grecs_dhcpdn <= 0)
    {
      if (grecs_dhcpdtable_value_is_error (grecs_dhcpdn))
        goto grecs_dhcpderrlab;
      grecs_dhcpdn = -grecs_dhcpdn;
      goto grecs_dhcpdreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (grecs_dhcpderrstatus)
    grecs_dhcpderrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", grecs_dhcpdtoken, &grecs_dhcpdlval, &grecs_dhcpdlloc);
  grecs_dhcpdstate = grecs_dhcpdn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_dhcpdvsp = grecs_dhcpdlval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++grecs_dhcpdlsp = grecs_dhcpdlloc;

  /* Discard the shifted token.  */
  grecs_dhcpdchar = YYEMPTY;
  goto grecs_dhcpdnewstate;


/*-----------------------------------------------------------.
| grecs_dhcpddefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
grecs_dhcpddefault:
  grecs_dhcpdn = grecs_dhcpddefact[grecs_dhcpdstate];
  if (grecs_dhcpdn == 0)
    goto grecs_dhcpderrlab;
  goto grecs_dhcpdreduce;


/*-----------------------------.
| grecs_dhcpdreduce -- do a reduction.  |
`-----------------------------*/
grecs_dhcpdreduce:
  /* grecs_dhcpdn is the number of a rule to reduce with.  */
  grecs_dhcpdlen = grecs_dhcpdr2[grecs_dhcpdn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  grecs_dhcpdval = grecs_dhcpdvsp[1-grecs_dhcpdlen];

  /* Default location. */
  YYLLOC_DEFAULT (grecs_dhcpdloc, (grecs_dhcpdlsp - grecs_dhcpdlen), grecs_dhcpdlen);
  grecs_dhcpderror_range[1] = grecs_dhcpdloc;
  YY_REDUCE_PRINT (grecs_dhcpdn);
  switch (grecs_dhcpdn)
    {
  case 2: /* input: maybe_stmtlist  */
#line 78 "dhcpd-gram.y"
          {
		  parse_tree = grecs_node_create(grecs_node_root, &(grecs_dhcpdlsp[0]));
		  parse_tree->v.texttab = grecs_text_table();
		  grecs_node_bind(parse_tree, (grecs_dhcpdvsp[0].node), 1);
	  }
#line 1645 "dhcpd-gram.c"
    break;

  case 3: /* maybe_stmtlist: %empty  */
#line 87 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = NULL;
	  }
#line 1653 "dhcpd-gram.c"
    break;

  case 4: /* maybe_stmtlist: stmtlist  */
#line 91 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = (grecs_dhcpdvsp[0].node_list).head;
	  }
#line 1661 "dhcpd-gram.c"
    break;

  case 5: /* stmtlist: stmt  */
#line 97 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node_list).head = (grecs_dhcpdval.node_list).tail = (grecs_dhcpdvsp[0].node);
	  }
#line 1669 "dhcpd-gram.c"
    break;

  case 6: /* stmtlist: stmtlist stmt  */
#line 101 "dhcpd-gram.y"
          {
		  if ((grecs_dhcpdvsp[0].node)) {
			  if (!(grecs_dhcpdvsp[-1].node_list).head)
				  (grecs_dhcpdvsp[-1].node_list).head = (grecs_dhcpdvsp[-1].node_list).tail = (grecs_dhcpdvsp[0].node);
			  else
				  grecs_node_bind((grecs_dhcpdvsp[-1].node_list).tail, (grecs_dhcpdvsp[0].node), 0);
		  }
		  (grecs_dhcpdval.node_list) = (grecs_dhcpdvsp[-1].node_list);
	  }
#line 1683 "dhcpd-gram.c"
    break;

  case 10: /* simple: DHCPD_IDENT vallist ';'  */
#line 118 "dhcpd-gram.y"
          {
		  if (strcmp((grecs_dhcpdvsp[-2].string), "include") == 0 &&
		      (grecs_dhcpdvsp[-1].pvalue)->type == GRECS_TYPE_STRING) {
			  grecs_dhcpd_new_source((grecs_dhcpdvsp[-1].pvalue)->v.string, &(grecs_dhcpdlsp[-2]));
			  (grecs_dhcpdval.node) = NULL;
		  } else {
			  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_stmt,
							(grecs_dhcpdlsp[-2]).beg, (grecs_dhcpdlsp[-1]).end);
			  (grecs_dhcpdval.node)->ident = (grecs_dhcpdvsp[-2].string);
			  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-2]);
			  (grecs_dhcpdval.node)->v.value = (grecs_dhcpdvsp[-1].pvalue);
		  }
	  }
#line 1701 "dhcpd-gram.c"
    break;

  case 11: /* $@1: %empty  */
#line 131 "dhcpd-gram.y"
                          { grecs_dhcpd_begin_expr(); }
#line 1707 "dhcpd-gram.c"
    break;

  case 12: /* simple: DHCPD_IDENT '=' $@1 DHCPD_EXPR ';'  */
#line 132 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_stmt,
						(grecs_dhcpdlsp[-4]).beg, (grecs_dhcpdlsp[0]).end);
		  (grecs_dhcpdval.node)->ident = (grecs_dhcpdvsp[-4].string);
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-4]);
		  (grecs_dhcpdval.node)->v.value = make_string_value((grecs_dhcpdvsp[-1].string));
	  }
#line 1719 "dhcpd-gram.c"
    break;

  case 13: /* simple: string ';'  */
#line 140 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create(grecs_node_stmt, &(grecs_dhcpdlsp[-1]));
		  (grecs_dhcpdval.node)->ident = (grecs_dhcpdvsp[-1].string);
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-1]);
		  (grecs_dhcpdval.node)->v.value = NULL;
	  }
#line 1730 "dhcpd-gram.c"
    break;

  case 14: /* block: DHCPD_IDENT tag '{' maybe_stmtlist '}' opt_semi  */
#line 149 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_block,
						(grecs_dhcpdlsp[-5]).beg, (grecs_dhcpdlsp[-1]).end);
		  (grecs_dhcpdval.node)->ident = (grecs_dhcpdvsp[-5].string);
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-5]);
		  (grecs_dhcpdval.node)->v.value = (grecs_dhcpdvsp[-4].pvalue);
		  grecs_node_bind((grecs_dhcpdval.node), (grecs_dhcpdvsp[-2].node), 1);
	  }
#line 1743 "dhcpd-gram.c"
    break;

  case 17: /* tag: %empty  */
#line 164 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.pvalue) = NULL;
	  }
#line 1751 "dhcpd-gram.c"
    break;

  case 19: /* vallist: vlist  */
#line 171 "dhcpd-gram.y"
          {
		  size_t n;
		  
		  if ((n = grecs_list_size((grecs_dhcpdvsp[0].list))) == 1) {
			  (grecs_dhcpdval.pvalue) = grecs_list_index((grecs_dhcpdvsp[0].list), 0);
		  } else {
			  size_t i;
			  struct grecs_list_entry *ep;
		
			  (grecs_dhcpdval.pvalue) = grecs_malloc(sizeof((grecs_dhcpdval.pvalue)[0]));
			  (grecs_dhcpdval.pvalue)->type = GRECS_TYPE_ARRAY;
			  (grecs_dhcpdval.pvalue)->locus = (grecs_dhcpdlsp[0]);
			  (grecs_dhcpdval.pvalue)->v.arg.c = n;
			  (grecs_dhcpdval.pvalue)->v.arg.v = grecs_calloc(n,
						     sizeof((grecs_dhcpdval.pvalue)->v.arg.v[0]));
			  for (i = 0, ep = (grecs_dhcpdvsp[0].list)->head; ep; i++, ep = ep->next)
				  (grecs_dhcpdval.pvalue)->v.arg.v[i] = ep->data;
		  }
		  (grecs_dhcpdvsp[0].list)->free_entry = NULL;
		  grecs_list_free((grecs_dhcpdvsp[0].list));	      
	  }
#line 1777 "dhcpd-gram.c"
    break;

  case 20: /* vlist: value  */
#line 195 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.list) = grecs_value_list_create();
		  grecs_list_append((grecs_dhcpdval.list), grecs_value_ptr_from_static(&(grecs_dhcpdvsp[0].svalue)));
	  }
#line 1786 "dhcpd-gram.c"
    break;

  case 21: /* vlist: vlist value  */
#line 200 "dhcpd-gram.y"
          {
		  grecs_list_append((grecs_dhcpdvsp[-1].list), grecs_value_ptr_from_static(&(grecs_dhcpdvsp[0].svalue)));
	  }
#line 1794 "dhcpd-gram.c"
    break;

  case 22: /* value: string  */
#line 206 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.svalue).type = GRECS_TYPE_STRING;
		  (grecs_dhcpdval.svalue).locus = (grecs_dhcpdlsp[0]);
		  (grecs_dhcpdval.svalue).v.string = (grecs_dhcpdvsp[0].string);
	  }
#line 1804 "dhcpd-gram.c"
    break;

  case 23: /* value: strlist  */
#line 212 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.svalue).type = GRECS_TYPE_LIST;
		  (grecs_dhcpdval.svalue).locus = (grecs_dhcpdlsp[0]);
		  (grecs_dhcpdval.svalue).v.list = (grecs_dhcpdvsp[0].list);
	  }
#line 1814 "dhcpd-gram.c"
    break;

  case 26: /* strlist: DHCPD_STRING ',' DHCPD_STRING  */
#line 224 "dhcpd-gram.y"
          {
		  grecs_value_t val;

		  (grecs_dhcpdval.list) = grecs_value_list_create();

		  val.type = GRECS_TYPE_STRING;
		  val.locus = (grecs_dhcpdlsp[-2]);
		  val.v.string = (grecs_dhcpdvsp[-2].string);
		  grecs_list_append((grecs_dhcpdval.list), grecs_value_ptr_from_static(&val));
		  
		  val.type = GRECS_TYPE_STRING;
		  val.locus = (grecs_dhcpdlsp[0]);
		  val.v.string = (grecs_dhcpdvsp[0].string);
		  grecs_list_append((grecs_dhcpdval.list), grecs_value_ptr_from_static(&val));
	  }
#line 1834 "dhcpd-gram.c"
    break;

  case 27: /* strlist: strlist ',' DHCPD_STRING  */
#line 240 "dhcpd-gram.y"
          {
		  grecs_value_t val;

		  val.type = GRECS_TYPE_STRING;
		  val.locus = (grecs_dhcpdlsp[0]);
		  val.v.string = (grecs_dhcpdvsp[0].string);
		  grecs_list_append((grecs_dhcpdvsp[-2].list), grecs_value_ptr_from_static(&val));
	  }
#line 1847 "dhcpd-gram.c"
    break;

  case 28: /* cond: if DHCPD_EXPR '{' maybe_stmtlist '}' opt_elsifchain opt_elsecond  */
#line 251 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_block,
						(grecs_dhcpdlsp[-6]).beg, (grecs_dhcpdlsp[0]).end);
		  
		  grecs_line_begin();
		  grecs_line_add("if", 2);

		  (grecs_dhcpdval.node)->ident = grecs_line_finish();
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-6]);
		  
		  (grecs_dhcpdval.node)->v.value = make_string_value ((grecs_dhcpdvsp[-5].string));
		  grecs_node_bind((grecs_dhcpdval.node), (grecs_dhcpdvsp[-3].node), 1);

		  if ((grecs_dhcpdvsp[-1].node_list).head) {
			  grecs_node_bind((grecs_dhcpdvsp[-1].node_list).tail, (grecs_dhcpdvsp[0].node), 0);
			  grecs_node_bind((grecs_dhcpdval.node), (grecs_dhcpdvsp[-1].node_list).head, 0);
		  }
	  }
#line 1870 "dhcpd-gram.c"
    break;

  case 29: /* if: DHCPD_IF  */
#line 272 "dhcpd-gram.y"
          {
		  grecs_dhcpd_begin_bool();
	  }
#line 1878 "dhcpd-gram.c"
    break;

  case 30: /* elsif: DHCPD_ELSIF  */
#line 278 "dhcpd-gram.y"
          {
		  grecs_dhcpd_begin_bool();
	  }
#line 1886 "dhcpd-gram.c"
    break;

  case 31: /* opt_elsifchain: %empty  */
#line 284 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node_list).head = (grecs_dhcpdval.node_list).tail = NULL;
	  }
#line 1894 "dhcpd-gram.c"
    break;

  case 33: /* elsifchain: elsifcond  */
#line 291 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node_list).head = (grecs_dhcpdval.node_list).tail = (grecs_dhcpdvsp[0].node);
	  }
#line 1902 "dhcpd-gram.c"
    break;

  case 34: /* elsifchain: elsifchain elsifcond  */
#line 295 "dhcpd-gram.y"
          {
		  if ((grecs_dhcpdvsp[0].node)) {
			  if (!(grecs_dhcpdvsp[-1].node_list).head)
				  (grecs_dhcpdvsp[-1].node_list).head = (grecs_dhcpdvsp[-1].node_list).tail = (grecs_dhcpdvsp[0].node);
			  else
				  grecs_node_bind((grecs_dhcpdvsp[-1].node_list).tail, (grecs_dhcpdvsp[0].node), 0);
		  }
		  (grecs_dhcpdval.node_list) = (grecs_dhcpdvsp[-1].node_list);
	  }
#line 1916 "dhcpd-gram.c"
    break;

  case 35: /* elsifcond: elsif DHCPD_EXPR '{' maybe_stmtlist '}'  */
#line 307 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_block,
						(grecs_dhcpdlsp[-4]).beg, (grecs_dhcpdlsp[0]).end);
		  
		  grecs_line_begin();
		  grecs_line_add("elsif", 5);

		  (grecs_dhcpdval.node)->ident = grecs_line_finish();
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-4]);
		  (grecs_dhcpdval.node)->v.value = make_string_value ((grecs_dhcpdvsp[-3].string));
		  grecs_node_bind((grecs_dhcpdval.node), (grecs_dhcpdvsp[-1].node), 1);
	  }
#line 1933 "dhcpd-gram.c"
    break;

  case 36: /* opt_elsecond: %empty  */
#line 322 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = NULL;
	  }
#line 1941 "dhcpd-gram.c"
    break;

  case 38: /* elsecond: DHCPD_ELSE '{' maybe_stmtlist '}'  */
#line 329 "dhcpd-gram.y"
          {
		  (grecs_dhcpdval.node) = grecs_node_create_points(grecs_node_block,
						(grecs_dhcpdlsp[-3]).beg, (grecs_dhcpdlsp[0]).end);
		  
		  grecs_line_begin();
		  grecs_line_add("else", 4);

		  (grecs_dhcpdval.node)->ident = grecs_line_finish();
		  (grecs_dhcpdval.node)->idloc = (grecs_dhcpdlsp[-3]);
		  (grecs_dhcpdval.node)->v.value = NULL;
		  grecs_node_bind((grecs_dhcpdval.node), (grecs_dhcpdvsp[-1].node), 1);
	  }
#line 1958 "dhcpd-gram.c"
    break;


#line 1962 "dhcpd-gram.c"

      default: break;
    }
  /* User semantic actions sometimes alter grecs_dhcpdchar, and that requires
     that grecs_dhcpdtoken be updated with the new translation.  We take the
     approach of translating immediately before every use of grecs_dhcpdtoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering grecs_dhcpdchar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (grecs_dhcpdsymbol_kind_t, grecs_dhcpdr1[grecs_dhcpdn]), &grecs_dhcpdval, &grecs_dhcpdloc);

  YYPOPSTACK (grecs_dhcpdlen);
  grecs_dhcpdlen = 0;

  *++grecs_dhcpdvsp = grecs_dhcpdval;
  *++grecs_dhcpdlsp = grecs_dhcpdloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int grecs_dhcpdlhs = grecs_dhcpdr1[grecs_dhcpdn] - YYNTOKENS;
    const int grecs_dhcpdi = grecs_dhcpdpgoto[grecs_dhcpdlhs] + *grecs_dhcpdssp;
    grecs_dhcpdstate = (0 <= grecs_dhcpdi && grecs_dhcpdi <= YYLAST && grecs_dhcpdcheck[grecs_dhcpdi] == *grecs_dhcpdssp
               ? grecs_dhcpdtable[grecs_dhcpdi]
               : grecs_dhcpddefgoto[grecs_dhcpdlhs]);
  }

  goto grecs_dhcpdnewstate;


/*--------------------------------------.
| grecs_dhcpderrlab -- here on detecting error.  |
`--------------------------------------*/
grecs_dhcpderrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  grecs_dhcpdtoken = grecs_dhcpdchar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (grecs_dhcpdchar);
  /* If not already recovering from an error, report this error.  */
  if (!grecs_dhcpderrstatus)
    {
      ++grecs_dhcpdnerrs;
      {
        grecs_dhcpdpcontext_t grecs_dhcpdctx
          = {grecs_dhcpdssp, grecs_dhcpdtoken, &grecs_dhcpdlloc};
        char const *grecs_dhcpdmsgp = YY_("syntax error");
        int grecs_dhcpdsyntax_error_status;
        grecs_dhcpdsyntax_error_status = grecs_dhcpdsyntax_error (&grecs_dhcpdmsg_alloc, &grecs_dhcpdmsg, &grecs_dhcpdctx);
        if (grecs_dhcpdsyntax_error_status == 0)
          grecs_dhcpdmsgp = grecs_dhcpdmsg;
        else if (grecs_dhcpdsyntax_error_status == -1)
          {
            if (grecs_dhcpdmsg != grecs_dhcpdmsgbuf)
              YYSTACK_FREE (grecs_dhcpdmsg);
            grecs_dhcpdmsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, grecs_dhcpdmsg_alloc)));
            if (grecs_dhcpdmsg)
              {
                grecs_dhcpdsyntax_error_status
                  = grecs_dhcpdsyntax_error (&grecs_dhcpdmsg_alloc, &grecs_dhcpdmsg, &grecs_dhcpdctx);
                grecs_dhcpdmsgp = grecs_dhcpdmsg;
              }
            else
              {
                grecs_dhcpdmsg = grecs_dhcpdmsgbuf;
                grecs_dhcpdmsg_alloc = sizeof grecs_dhcpdmsgbuf;
                grecs_dhcpdsyntax_error_status = YYENOMEM;
              }
          }
        grecs_dhcpderror (grecs_dhcpdmsgp);
        if (grecs_dhcpdsyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  grecs_dhcpderror_range[1] = grecs_dhcpdlloc;
  if (grecs_dhcpderrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (grecs_dhcpdchar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (grecs_dhcpdchar == YYEOF)
            YYABORT;
        }
      else
        {
          grecs_dhcpddestruct ("Error: discarding",
                      grecs_dhcpdtoken, &grecs_dhcpdlval, &grecs_dhcpdlloc);
          grecs_dhcpdchar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto grecs_dhcpderrlab1;


/*---------------------------------------------------.
| grecs_dhcpderrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
grecs_dhcpderrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label grecs_dhcpderrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++grecs_dhcpdnerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (grecs_dhcpdlen);
  grecs_dhcpdlen = 0;
  YY_STACK_PRINT (grecs_dhcpdss, grecs_dhcpdssp);
  grecs_dhcpdstate = *grecs_dhcpdssp;
  goto grecs_dhcpderrlab1;


/*-------------------------------------------------------------.
| grecs_dhcpderrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
grecs_dhcpderrlab1:
  grecs_dhcpderrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      grecs_dhcpdn = grecs_dhcpdpact[grecs_dhcpdstate];
      if (!grecs_dhcpdpact_value_is_default (grecs_dhcpdn))
        {
          grecs_dhcpdn += YYSYMBOL_YYerror;
          if (0 <= grecs_dhcpdn && grecs_dhcpdn <= YYLAST && grecs_dhcpdcheck[grecs_dhcpdn] == YYSYMBOL_YYerror)
            {
              grecs_dhcpdn = grecs_dhcpdtable[grecs_dhcpdn];
              if (0 < grecs_dhcpdn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (grecs_dhcpdssp == grecs_dhcpdss)
        YYABORT;

      grecs_dhcpderror_range[1] = *grecs_dhcpdlsp;
      grecs_dhcpddestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (grecs_dhcpdstate), grecs_dhcpdvsp, grecs_dhcpdlsp);
      YYPOPSTACK (1);
      grecs_dhcpdstate = *grecs_dhcpdssp;
      YY_STACK_PRINT (grecs_dhcpdss, grecs_dhcpdssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_dhcpdvsp = grecs_dhcpdlval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  grecs_dhcpderror_range[2] = grecs_dhcpdlloc;
  ++grecs_dhcpdlsp;
  YYLLOC_DEFAULT (*grecs_dhcpdlsp, grecs_dhcpderror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (grecs_dhcpdn), grecs_dhcpdvsp, grecs_dhcpdlsp);

  grecs_dhcpdstate = grecs_dhcpdn;
  goto grecs_dhcpdnewstate;


/*-------------------------------------.
| grecs_dhcpdacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
grecs_dhcpdacceptlab:
  grecs_dhcpdresult = 0;
  goto grecs_dhcpdreturnlab;


/*-----------------------------------.
| grecs_dhcpdabortlab -- YYABORT comes here.  |
`-----------------------------------*/
grecs_dhcpdabortlab:
  grecs_dhcpdresult = 1;
  goto grecs_dhcpdreturnlab;


/*-----------------------------------------------------------.
| grecs_dhcpdexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
grecs_dhcpdexhaustedlab:
  grecs_dhcpderror (YY_("memory exhausted"));
  grecs_dhcpdresult = 2;
  goto grecs_dhcpdreturnlab;


/*----------------------------------------------------------.
| grecs_dhcpdreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
grecs_dhcpdreturnlab:
  if (grecs_dhcpdchar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      grecs_dhcpdtoken = YYTRANSLATE (grecs_dhcpdchar);
      grecs_dhcpddestruct ("Cleanup: discarding lookahead",
                  grecs_dhcpdtoken, &grecs_dhcpdlval, &grecs_dhcpdlloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (grecs_dhcpdlen);
  YY_STACK_PRINT (grecs_dhcpdss, grecs_dhcpdssp);
  while (grecs_dhcpdssp != grecs_dhcpdss)
    {
      grecs_dhcpddestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*grecs_dhcpdssp), grecs_dhcpdvsp, grecs_dhcpdlsp);
      YYPOPSTACK (1);
    }
#ifndef grecs_dhcpdoverflow
  if (grecs_dhcpdss != grecs_dhcpdssa)
    YYSTACK_FREE (grecs_dhcpdss);
#endif
  if (grecs_dhcpdmsg != grecs_dhcpdmsgbuf)
    YYSTACK_FREE (grecs_dhcpdmsg);
  return grecs_dhcpdresult;
}

#line 343 "dhcpd-gram.y"


int
grecs_dhcpderror(char const *s)
{
	grecs_error(&grecs_dhcpdlloc, 0, "%s", s);
	return 0;
}

struct grecs_node *
grecs_dhcpd_parser(const char *name, int traceflags)
{
	int rc;

	if (grecs_dhcpd_new_source(name, NULL))
		return NULL;
	grecs_dhcpd_flex_debug = traceflags & GRECS_TRACE_LEX;
	grecs_dhcpddebug = traceflags & GRECS_TRACE_GRAM;
	parse_tree = NULL;
	grecs_line_acc_create();
	rc = grecs_dhcpdparse();
	grecs_dhcpd_close_sources();
	if (grecs_error_count)
		rc = 1;
	grecs_line_acc_free();
	if (rc) {
		grecs_tree_free(parse_tree);
		parse_tree = NULL;
	}
	return parse_tree;
}

