/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or grecs_bind__.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with grecs_bind_ or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "bind-gram.y"

/* grecs - Gray's Extensible Configuration System
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Grecs is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Grecs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with Grecs. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <grecs.h>
#include <bind-gram.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

int grecs_bind_lex(void);
int grecs_bind_error(char const *s);

static struct grecs_node *parse_tree;
extern int grecs_bind__flex_debug;
extern int grecs_bind_new_source(const char *name, grecs_locus_t *loc);
extern void grecs_bind_close_sources(void);

static struct grecs_value *stmtlist_to_value(struct grecs_node *node);

#line 109 "bind-gram.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_BIND_GRAM_H_INCLUDED
# define YY_YY_BIND_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int grecs_bind_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum grecs_bind_tokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    BIND_STRING = 258,             /* BIND_STRING  */
    BIND_IDENT = 259,              /* BIND_IDENT  */
    BIND_CONTROLS = 260            /* BIND_CONTROLS  */
  };
  typedef enum grecs_bind_tokentype grecs_bind_token_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define BIND_STRING 258
#define BIND_IDENT 259
#define BIND_CONTROLS 260

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 42 "bind-gram.y"

	char *string;
	grecs_value_t svalue, *pvalue;
	struct grecs_list *list;
	struct grecs_node *node;
	grecs_locus_t locus;
	struct { struct grecs_node *head, *tail; } node_list;

#line 181 "bind-gram.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE grecs_bind_lval;
extern YYLTYPE grecs_bind_lloc;

int grecs_bind_parse (void);


#endif /* !YY_YY_BIND_GRAM_H_INCLUDED  */
/* Symbol kind.  */
enum grecs_bind_symbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_BIND_STRING = 3,                /* BIND_STRING  */
  YYSYMBOL_BIND_IDENT = 4,                 /* BIND_IDENT  */
  YYSYMBOL_BIND_CONTROLS = 5,              /* BIND_CONTROLS  */
  YYSYMBOL_6_ = 6,                         /* ';'  */
  YYSYMBOL_7_ = 7,                         /* '!'  */
  YYSYMBOL_8_ = 8,                         /* '{'  */
  YYSYMBOL_9_ = 9,                         /* '}'  */
  YYSYMBOL_YYACCEPT = 10,                  /* $accept  */
  YYSYMBOL_input = 11,                     /* input  */
  YYSYMBOL_maybe_stmtlist = 12,            /* maybe_stmtlist  */
  YYSYMBOL_stmtlist = 13,                  /* stmtlist  */
  YYSYMBOL_stmt = 14,                      /* stmt  */
  YYSYMBOL_simple = 15,                    /* simple  */
  YYSYMBOL_block = 16,                     /* block  */
  YYSYMBOL_tag = 17,                       /* tag  */
  YYSYMBOL_vallist = 18,                   /* vallist  */
  YYSYMBOL_vlist = 19,                     /* vlist  */
  YYSYMBOL_value = 20,                     /* value  */
  YYSYMBOL_string = 21,                    /* string  */
  YYSYMBOL_ctlsub = 22,                    /* ctlsub  */
  YYSYMBOL_ctllist = 23                    /* ctllist  */
};
typedef enum grecs_bind_symbol_kind_t grecs_bind_symbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ grecs_bind_type_int8;
#elif defined YY_STDINT_H
typedef int_least8_t grecs_bind_type_int8;
#else
typedef signed char grecs_bind_type_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ grecs_bind_type_int16;
#elif defined YY_STDINT_H
typedef int_least16_t grecs_bind_type_int16;
#else
typedef short grecs_bind_type_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ grecs_bind_type_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t grecs_bind_type_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char grecs_bind_type_uint8;
#else
typedef short grecs_bind_type_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ grecs_bind_type_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t grecs_bind_type_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short grecs_bind_type_uint16;
#else
typedef int grecs_bind_type_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef grecs_bind_type_int8 grecs_bind__state_t;

/* State numbers in computations.  */
typedef int grecs_bind__state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about grecs_bind_lval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined grecs_bind_overflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union grecs_bind_alloc
{
  grecs_bind__state_t grecs_bind_ss_alloc;
  YYSTYPE grecs_bind_vs_alloc;
  YYLTYPE grecs_bind_ls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union grecs_bind_alloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (grecs_bind__state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T grecs_bind_newbytes;                                         \
        YYCOPY (&grecs_bind_ptr->Stack_alloc, Stack, grecs_bind_size);                    \
        Stack = &grecs_bind_ptr->Stack_alloc;                                    \
        grecs_bind_newbytes = grecs_bind_stacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        grecs_bind_ptr += grecs_bind_newbytes / YYSIZEOF (*grecs_bind_ptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T grecs_bind_i;                      \
          for (grecs_bind_i = 0; grecs_bind_i < (Count); grecs_bind_i++)   \
            (Dst)[grecs_bind_i] = (Src)[grecs_bind_i];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  20
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   43

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  10
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  14
/* YYNRULES -- Number of rules.  */
#define YYNRULES  25
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  40

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   260


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_bind_lex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (grecs_bind_symbol_kind_t, grecs_bind_translate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_bind_lex.  */
static const grecs_bind_type_int8 grecs_bind_translate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     7,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     6,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     8,     2,     9,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const grecs_bind_type_uint8 grecs_bind_rline[] =
{
       0,    61,    61,    71,    74,    80,    84,    96,    97,   100,
     114,   121,   134,   143,   166,   169,   172,   196,   201,   207,
     215,   216,   219,   227,   232,   236
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (grecs_bind_symbol_kind_t, grecs_bind_stos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *grecs_bind_symbol_name (grecs_bind_symbol_kind_t grecs_bind_symbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const grecs_bind_tname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "BIND_STRING",
  "BIND_IDENT", "BIND_CONTROLS", "';'", "'!'", "'{'", "'}'", "$accept",
  "input", "maybe_stmtlist", "stmtlist", "stmt", "simple", "block", "tag",
  "vallist", "vlist", "value", "string", "ctlsub", "ctllist", YY_NULLPTR
};

static const char *
grecs_bind_symbol_name (grecs_bind_symbol_kind_t grecs_bind_symbol)
{
  return grecs_bind_tname[grecs_bind_symbol];
}
#endif

#define YYPACT_NINF (-14)

#define grecs_bind_pact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-22)

#define grecs_bind_table_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const grecs_bind_type_int8 grecs_bind_pact[] =
{
      21,   -14,    28,    -4,    32,     9,   -14,    21,   -14,   -14,
     -14,     5,   -14,     8,     7,    32,   -14,   -14,    32,    14,
     -14,   -14,   -14,    21,   -14,   -14,   -14,    13,    15,   -14,
      20,    27,   -14,    21,   -14,    31,   -14,     3,   -14,   -14
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const grecs_bind_type_int8 grecs_bind_defact[] =
{
       3,    20,    14,     0,     0,     0,     2,     4,     5,     7,
       8,     0,    21,     0,    15,    16,    17,    19,     0,     0,
       1,     6,    10,     3,     9,    18,    23,     0,     0,    11,
       0,     0,    22,     0,    24,     0,    13,     0,    12,    25
};

/* YYPGOTO[NTERM-NUM].  */
static const grecs_bind_type_int8 grecs_bind_pgoto[] =
{
     -14,   -14,    16,    10,    -7,   -14,   -14,   -14,   -14,   -14,
     -13,    -1,   -14,   -14
};

/* YYDEFGOTO[NTERM-NUM].  */
static const grecs_bind_type_int8 grecs_bind_defgoto[] =
{
       0,     5,     6,     7,     8,     9,    10,    13,    14,    15,
      16,    11,    27,    28
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const grecs_bind_type_int8 grecs_bind_table[] =
{
      21,    17,    25,    19,    18,    26,     1,     2,     3,    20,
       4,    22,    39,    24,    17,    34,    23,    17,     1,    12,
      29,    32,    31,    33,     1,     2,     3,    17,     4,    35,
      21,     1,    12,    36,   -21,     1,    12,    38,     0,    30,
       0,     0,     0,    37
};

static const grecs_bind_type_int8 grecs_bind_check[] =
{
       7,     2,    15,     4,     8,    18,     3,     4,     5,     0,
       7,     6,     9,     6,    15,    28,     8,    18,     3,     4,
       6,     6,     9,     8,     3,     4,     5,    28,     7,     9,
      37,     3,     4,     6,     6,     3,     4,     6,    -1,    23,
      -1,    -1,    -1,    33
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const grecs_bind_type_int8 grecs_bind_stos[] =
{
       0,     3,     4,     5,     7,    11,    12,    13,    14,    15,
      16,    21,     4,    17,    18,    19,    20,    21,     8,    21,
       0,    14,     6,     8,     6,    20,    20,    22,    23,     6,
      12,     9,     6,     8,    20,     9,     6,    13,     6,     9
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const grecs_bind_type_int8 grecs_bind_r1[] =
{
       0,    10,    11,    12,    12,    13,    13,    14,    14,    15,
      15,    15,    16,    16,    17,    17,    18,    19,    19,    20,
      21,    21,    22,    23,    23,    23
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const grecs_bind_type_int8 grecs_bind_r2[] =
{
       0,     2,     1,     0,     1,     1,     2,     1,     1,     3,
       2,     3,     6,     5,     0,     1,     1,     1,     2,     1,
       1,     1,     2,     1,     2,     4
};


enum { YYENOMEM = -2 };

#define grecs_bind_errok         (grecs_bind_errstatus = 0)
#define grecs_bind_clearin       (grecs_bind_char = YYEMPTY)

#define YYACCEPT        goto grecs_bind_acceptlab
#define YYABORT         goto grecs_bind_abortlab
#define YYERROR         goto grecs_bind_errorlab
#define YYNOMEM         goto grecs_bind_exhaustedlab


#define YYRECOVERING()  (!!grecs_bind_errstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (grecs_bind_char == YYEMPTY)                                        \
      {                                                           \
        grecs_bind_char = (Token);                                         \
        grecs_bind_lval = (Value);                                         \
        YYPOPSTACK (grecs_bind_len);                                       \
        grecs_bind_state = *grecs_bind_ssp;                                         \
        goto grecs_bind_backup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        grecs_bind_error (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (grecs_bind_debug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
grecs_bind__location_print_ (FILE *grecs_bind_o, YYLTYPE const * const grecs_bind_locp)
{
  int res = 0;
  int end_col = 0 != grecs_bind_locp->last_column ? grecs_bind_locp->last_column - 1 : 0;
  if (0 <= grecs_bind_locp->first_line)
    {
      res += YYFPRINTF (grecs_bind_o, "%d", grecs_bind_locp->first_line);
      if (0 <= grecs_bind_locp->first_column)
        res += YYFPRINTF (grecs_bind_o, ".%d", grecs_bind_locp->first_column);
    }
  if (0 <= grecs_bind_locp->last_line)
    {
      if (grecs_bind_locp->first_line < grecs_bind_locp->last_line)
        {
          res += YYFPRINTF (grecs_bind_o, "-%d", grecs_bind_locp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (grecs_bind_o, ".%d", end_col);
        }
      else if (0 <= end_col && grecs_bind_locp->first_column < end_col)
        res += YYFPRINTF (grecs_bind_o, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  grecs_bind__location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (grecs_bind_debug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      grecs_bind__symbol_print (stderr,                                            \
                  Kind, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
grecs_bind__symbol_value_print (FILE *grecs_bind_o,
                       grecs_bind_symbol_kind_t grecs_bind_kind, YYSTYPE const * const grecs_bind_valuep, YYLTYPE const * const grecs_bind_locationp)
{
  FILE *grecs_bind_output = grecs_bind_o;
  YY_USE (grecs_bind_output);
  YY_USE (grecs_bind_locationp);
  if (!grecs_bind_valuep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_bind_kind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
grecs_bind__symbol_print (FILE *grecs_bind_o,
                 grecs_bind_symbol_kind_t grecs_bind_kind, YYSTYPE const * const grecs_bind_valuep, YYLTYPE const * const grecs_bind_locationp)
{
  YYFPRINTF (grecs_bind_o, "%s %s (",
             grecs_bind_kind < YYNTOKENS ? "token" : "nterm", grecs_bind_symbol_name (grecs_bind_kind));

  YYLOCATION_PRINT (grecs_bind_o, grecs_bind_locationp);
  YYFPRINTF (grecs_bind_o, ": ");
  grecs_bind__symbol_value_print (grecs_bind_o, grecs_bind_kind, grecs_bind_valuep, grecs_bind_locationp);
  YYFPRINTF (grecs_bind_o, ")");
}

/*------------------------------------------------------------------.
| grecs_bind__stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
grecs_bind__stack_print (grecs_bind__state_t *grecs_bind_bottom, grecs_bind__state_t *grecs_bind_top)
{
  YYFPRINTF (stderr, "Stack now");
  for (; grecs_bind_bottom <= grecs_bind_top; grecs_bind_bottom++)
    {
      int grecs_bind_bot = *grecs_bind_bottom;
      YYFPRINTF (stderr, " %d", grecs_bind_bot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (grecs_bind_debug)                                                  \
    grecs_bind__stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
grecs_bind__reduce_print (grecs_bind__state_t *grecs_bind_ssp, YYSTYPE *grecs_bind_vsp, YYLTYPE *grecs_bind_lsp,
                 int grecs_bind_rule)
{
  int grecs_bind_lno = grecs_bind_rline[grecs_bind_rule];
  int grecs_bind_nrhs = grecs_bind_r2[grecs_bind_rule];
  int grecs_bind_i;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             grecs_bind_rule - 1, grecs_bind_lno);
  /* The symbols being reduced.  */
  for (grecs_bind_i = 0; grecs_bind_i < grecs_bind_nrhs; grecs_bind_i++)
    {
      YYFPRINTF (stderr, "   $%d = ", grecs_bind_i + 1);
      grecs_bind__symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+grecs_bind_ssp[grecs_bind_i + 1 - grecs_bind_nrhs]),
                       &grecs_bind_vsp[(grecs_bind_i + 1) - (grecs_bind_nrhs)],
                       &(grecs_bind_lsp[(grecs_bind_i + 1) - (grecs_bind_nrhs)]));
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (grecs_bind_debug)                          \
    grecs_bind__reduce_print (grecs_bind_ssp, grecs_bind_vsp, grecs_bind_lsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int grecs_bind_debug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  grecs_bind__state_t *grecs_bind_ssp;
  grecs_bind_symbol_kind_t grecs_bind_token;
  YYLTYPE *grecs_bind_lloc;
} grecs_bind_pcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
grecs_bind_pcontext_expected_tokens (const grecs_bind_pcontext_t *grecs_bind_ctx,
                            grecs_bind_symbol_kind_t grecs_bind_arg[], int grecs_bind_argn)
{
  /* Actual size of YYARG. */
  int grecs_bind_count = 0;
  int grecs_bind_n = grecs_bind_pact[+*grecs_bind_ctx->grecs_bind_ssp];
  if (!grecs_bind_pact_value_is_default (grecs_bind_n))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int grecs_bind_xbegin = grecs_bind_n < 0 ? -grecs_bind_n : 0;
      /* Stay within bounds of both grecs_bind_check and grecs_bind_tname.  */
      int grecs_bind_checklim = YYLAST - grecs_bind_n + 1;
      int grecs_bind_xend = grecs_bind_checklim < YYNTOKENS ? grecs_bind_checklim : YYNTOKENS;
      int grecs_bind_x;
      for (grecs_bind_x = grecs_bind_xbegin; grecs_bind_x < grecs_bind_xend; ++grecs_bind_x)
        if (grecs_bind_check[grecs_bind_x + grecs_bind_n] == grecs_bind_x && grecs_bind_x != YYSYMBOL_YYerror
            && !grecs_bind_table_value_is_error (grecs_bind_table[grecs_bind_x + grecs_bind_n]))
          {
            if (!grecs_bind_arg)
              ++grecs_bind_count;
            else if (grecs_bind_count == grecs_bind_argn)
              return 0;
            else
              grecs_bind_arg[grecs_bind_count++] = YY_CAST (grecs_bind_symbol_kind_t, grecs_bind_x);
          }
    }
  if (grecs_bind_arg && grecs_bind_count == 0 && 0 < grecs_bind_argn)
    grecs_bind_arg[0] = YYSYMBOL_YYEMPTY;
  return grecs_bind_count;
}




#ifndef grecs_bind_strlen
# if defined __GLIBC__ && defined _STRING_H
#  define grecs_bind_strlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
grecs_bind_strlen (const char *grecs_bind_str)
{
  YYPTRDIFF_T grecs_bind_len;
  for (grecs_bind_len = 0; grecs_bind_str[grecs_bind_len]; grecs_bind_len++)
    continue;
  return grecs_bind_len;
}
# endif
#endif

#ifndef grecs_bind_stpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define grecs_bind_stpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
grecs_bind_stpcpy (char *grecs_bind_dest, const char *grecs_bind_src)
{
  char *grecs_bind_d = grecs_bind_dest;
  const char *grecs_bind_s = grecs_bind_src;

  while ((*grecs_bind_d++ = *grecs_bind_s++) != '\0')
    continue;

  return grecs_bind_d - 1;
}
# endif
#endif

#ifndef grecs_bind_tnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for grecs_bind_error.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from grecs_bind_tname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
grecs_bind_tnamerr (char *grecs_bind_res, const char *grecs_bind_str)
{
  if (*grecs_bind_str == '"')
    {
      YYPTRDIFF_T grecs_bind_n = 0;
      char const *grecs_bind_p = grecs_bind_str;
      for (;;)
        switch (*++grecs_bind_p)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++grecs_bind_p != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (grecs_bind_res)
              grecs_bind_res[grecs_bind_n] = *grecs_bind_p;
            grecs_bind_n++;
            break;

          case '"':
            if (grecs_bind_res)
              grecs_bind_res[grecs_bind_n] = '\0';
            return grecs_bind_n;
          }
    do_not_strip_quotes: ;
    }

  if (grecs_bind_res)
    return grecs_bind_stpcpy (grecs_bind_res, grecs_bind_str) - grecs_bind_res;
  else
    return grecs_bind_strlen (grecs_bind_str);
}
#endif


static int
grecs_bind__syntax_error_arguments (const grecs_bind_pcontext_t *grecs_bind_ctx,
                           grecs_bind_symbol_kind_t grecs_bind_arg[], int grecs_bind_argn)
{
  /* Actual size of YYARG. */
  int grecs_bind_count = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in grecs_bind_char) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated grecs_bind_char.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (grecs_bind_ctx->grecs_bind_token != YYSYMBOL_YYEMPTY)
    {
      int grecs_bind_n;
      if (grecs_bind_arg)
        grecs_bind_arg[grecs_bind_count] = grecs_bind_ctx->grecs_bind_token;
      ++grecs_bind_count;
      grecs_bind_n = grecs_bind_pcontext_expected_tokens (grecs_bind_ctx,
                                        grecs_bind_arg ? grecs_bind_arg + 1 : grecs_bind_arg, grecs_bind_argn - 1);
      if (grecs_bind_n == YYENOMEM)
        return YYENOMEM;
      else
        grecs_bind_count += grecs_bind_n;
    }
  return grecs_bind_count;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
grecs_bind_syntax_error (YYPTRDIFF_T *grecs_bind_msg_alloc, char **grecs_bind_msg,
                const grecs_bind_pcontext_t *grecs_bind_ctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *grecs_bind_format = YY_NULLPTR;
  /* Arguments of grecs_bind_format: reported tokens (one for the "unexpected",
     one per "expected"). */
  grecs_bind_symbol_kind_t grecs_bind_arg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T grecs_bind_size = 0;

  /* Actual size of YYARG. */
  int grecs_bind_count = grecs_bind__syntax_error_arguments (grecs_bind_ctx, grecs_bind_arg, YYARGS_MAX);
  if (grecs_bind_count == YYENOMEM)
    return YYENOMEM;

  switch (grecs_bind_count)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        grecs_bind_format = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  grecs_bind_size = grecs_bind_strlen (grecs_bind_format) - 2 * grecs_bind_count + 1;
  {
    int grecs_bind_i;
    for (grecs_bind_i = 0; grecs_bind_i < grecs_bind_count; ++grecs_bind_i)
      {
        YYPTRDIFF_T grecs_bind_size1
          = grecs_bind_size + grecs_bind_tnamerr (YY_NULLPTR, grecs_bind_tname[grecs_bind_arg[grecs_bind_i]]);
        if (grecs_bind_size <= grecs_bind_size1 && grecs_bind_size1 <= YYSTACK_ALLOC_MAXIMUM)
          grecs_bind_size = grecs_bind_size1;
        else
          return YYENOMEM;
      }
  }

  if (*grecs_bind_msg_alloc < grecs_bind_size)
    {
      *grecs_bind_msg_alloc = 2 * grecs_bind_size;
      if (! (grecs_bind_size <= *grecs_bind_msg_alloc
             && *grecs_bind_msg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *grecs_bind_msg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *grecs_bind_p = *grecs_bind_msg;
    int grecs_bind_i = 0;
    while ((*grecs_bind_p = *grecs_bind_format) != '\0')
      if (*grecs_bind_p == '%' && grecs_bind_format[1] == 's' && grecs_bind_i < grecs_bind_count)
        {
          grecs_bind_p += grecs_bind_tnamerr (grecs_bind_p, grecs_bind_tname[grecs_bind_arg[grecs_bind_i++]]);
          grecs_bind_format += 2;
        }
      else
        {
          ++grecs_bind_p;
          ++grecs_bind_format;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
grecs_bind_destruct (const char *grecs_bind_msg,
            grecs_bind_symbol_kind_t grecs_bind_kind, YYSTYPE *grecs_bind_valuep, YYLTYPE *grecs_bind_locationp)
{
  YY_USE (grecs_bind_valuep);
  YY_USE (grecs_bind_locationp);
  if (!grecs_bind_msg)
    grecs_bind_msg = "Deleting";
  YY_SYMBOL_PRINT (grecs_bind_msg, grecs_bind_kind, grecs_bind_valuep, grecs_bind_locationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_bind_kind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int grecs_bind_char;

/* The semantic value of the lookahead symbol.  */
YYSTYPE grecs_bind_lval;
/* Location data for the lookahead symbol.  */
YYLTYPE grecs_bind_lloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int grecs_bind_nerrs;




/*----------.
| grecs_bind_parse.  |
`----------*/

int
grecs_bind_parse (void)
{
    grecs_bind__state_fast_t grecs_bind_state = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int grecs_bind_errstatus = 0;

    /* Refer to the stacks through separate pointers, to allow grecs_bind_overflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T grecs_bind_stacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    grecs_bind__state_t grecs_bind_ssa[YYINITDEPTH];
    grecs_bind__state_t *grecs_bind_ss = grecs_bind_ssa;
    grecs_bind__state_t *grecs_bind_ssp = grecs_bind_ss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE grecs_bind_vsa[YYINITDEPTH];
    YYSTYPE *grecs_bind_vs = grecs_bind_vsa;
    YYSTYPE *grecs_bind_vsp = grecs_bind_vs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE grecs_bind_lsa[YYINITDEPTH];
    YYLTYPE *grecs_bind_ls = grecs_bind_lsa;
    YYLTYPE *grecs_bind_lsp = grecs_bind_ls;

  int grecs_bind_n;
  /* The return value of grecs_bind_parse.  */
  int grecs_bind_result;
  /* Lookahead symbol kind.  */
  grecs_bind_symbol_kind_t grecs_bind_token = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE grecs_bind_val;
  YYLTYPE grecs_bind_loc;

  /* The locations where the error started and ended.  */
  YYLTYPE grecs_bind_error_range[3];

  /* Buffer for error messages, and its allocated size.  */
  char grecs_bind_msgbuf[128];
  char *grecs_bind_msg = grecs_bind_msgbuf;
  YYPTRDIFF_T grecs_bind_msg_alloc = sizeof grecs_bind_msgbuf;

#define YYPOPSTACK(N)   (grecs_bind_vsp -= (N), grecs_bind_ssp -= (N), grecs_bind_lsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int grecs_bind_len = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  grecs_bind_char = YYEMPTY; /* Cause a token to be read.  */

  grecs_bind_lsp[0] = grecs_bind_lloc;
  goto grecs_bind_setstate;


/*------------------------------------------------------------.
| grecs_bind_newstate -- push a new state, which is found in grecs_bind_state.  |
`------------------------------------------------------------*/
grecs_bind_newstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  grecs_bind_ssp++;


/*--------------------------------------------------------------------.
| grecs_bind_setstate -- set current state (the top of the stack) to grecs_bind_state.  |
`--------------------------------------------------------------------*/
grecs_bind_setstate:
  YYDPRINTF ((stderr, "Entering state %d\n", grecs_bind_state));
  YY_ASSERT (0 <= grecs_bind_state && grecs_bind_state < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *grecs_bind_ssp = YY_CAST (grecs_bind__state_t, grecs_bind_state);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (grecs_bind_ss, grecs_bind_ssp);

  if (grecs_bind_ss + grecs_bind_stacksize - 1 <= grecs_bind_ssp)
#if !defined grecs_bind_overflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T grecs_bind_size = grecs_bind_ssp - grecs_bind_ss + 1;

# if defined grecs_bind_overflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        grecs_bind__state_t *grecs_bind_ss1 = grecs_bind_ss;
        YYSTYPE *grecs_bind_vs1 = grecs_bind_vs;
        YYLTYPE *grecs_bind_ls1 = grecs_bind_ls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if grecs_bind_overflow is a macro.  */
        grecs_bind_overflow (YY_("memory exhausted"),
                    &grecs_bind_ss1, grecs_bind_size * YYSIZEOF (*grecs_bind_ssp),
                    &grecs_bind_vs1, grecs_bind_size * YYSIZEOF (*grecs_bind_vsp),
                    &grecs_bind_ls1, grecs_bind_size * YYSIZEOF (*grecs_bind_lsp),
                    &grecs_bind_stacksize);
        grecs_bind_ss = grecs_bind_ss1;
        grecs_bind_vs = grecs_bind_vs1;
        grecs_bind_ls = grecs_bind_ls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= grecs_bind_stacksize)
        YYNOMEM;
      grecs_bind_stacksize *= 2;
      if (YYMAXDEPTH < grecs_bind_stacksize)
        grecs_bind_stacksize = YYMAXDEPTH;

      {
        grecs_bind__state_t *grecs_bind_ss1 = grecs_bind_ss;
        union grecs_bind_alloc *grecs_bind_ptr =
          YY_CAST (union grecs_bind_alloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (grecs_bind_stacksize))));
        if (! grecs_bind_ptr)
          YYNOMEM;
        YYSTACK_RELOCATE (grecs_bind_ss_alloc, grecs_bind_ss);
        YYSTACK_RELOCATE (grecs_bind_vs_alloc, grecs_bind_vs);
        YYSTACK_RELOCATE (grecs_bind_ls_alloc, grecs_bind_ls);
#  undef YYSTACK_RELOCATE
        if (grecs_bind_ss1 != grecs_bind_ssa)
          YYSTACK_FREE (grecs_bind_ss1);
      }
# endif

      grecs_bind_ssp = grecs_bind_ss + grecs_bind_size - 1;
      grecs_bind_vsp = grecs_bind_vs + grecs_bind_size - 1;
      grecs_bind_lsp = grecs_bind_ls + grecs_bind_size - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, grecs_bind_stacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (grecs_bind_ss + grecs_bind_stacksize - 1 <= grecs_bind_ssp)
        YYABORT;
    }
#endif /* !defined grecs_bind_overflow && !defined YYSTACK_RELOCATE */


  if (grecs_bind_state == YYFINAL)
    YYACCEPT;

  goto grecs_bind_backup;


/*-----------.
| grecs_bind_backup.  |
`-----------*/
grecs_bind_backup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  grecs_bind_n = grecs_bind_pact[grecs_bind_state];
  if (grecs_bind_pact_value_is_default (grecs_bind_n))
    goto grecs_bind_default;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (grecs_bind_char == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      grecs_bind_char = grecs_bind_lex ();
    }

  if (grecs_bind_char <= YYEOF)
    {
      grecs_bind_char = YYEOF;
      grecs_bind_token = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (grecs_bind_char == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      grecs_bind_char = YYUNDEF;
      grecs_bind_token = YYSYMBOL_YYerror;
      grecs_bind_error_range[1] = grecs_bind_lloc;
      goto grecs_bind_errlab1;
    }
  else
    {
      grecs_bind_token = YYTRANSLATE (grecs_bind_char);
      YY_SYMBOL_PRINT ("Next token is", grecs_bind_token, &grecs_bind_lval, &grecs_bind_lloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  grecs_bind_n += grecs_bind_token;
  if (grecs_bind_n < 0 || YYLAST < grecs_bind_n || grecs_bind_check[grecs_bind_n] != grecs_bind_token)
    goto grecs_bind_default;
  grecs_bind_n = grecs_bind_table[grecs_bind_n];
  if (grecs_bind_n <= 0)
    {
      if (grecs_bind_table_value_is_error (grecs_bind_n))
        goto grecs_bind_errlab;
      grecs_bind_n = -grecs_bind_n;
      goto grecs_bind_reduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (grecs_bind_errstatus)
    grecs_bind_errstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", grecs_bind_token, &grecs_bind_lval, &grecs_bind_lloc);
  grecs_bind_state = grecs_bind_n;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_bind_vsp = grecs_bind_lval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++grecs_bind_lsp = grecs_bind_lloc;

  /* Discard the shifted token.  */
  grecs_bind_char = YYEMPTY;
  goto grecs_bind_newstate;


/*-----------------------------------------------------------.
| grecs_bind_default -- do the default action for the current state.  |
`-----------------------------------------------------------*/
grecs_bind_default:
  grecs_bind_n = grecs_bind_defact[grecs_bind_state];
  if (grecs_bind_n == 0)
    goto grecs_bind_errlab;
  goto grecs_bind_reduce;


/*-----------------------------.
| grecs_bind_reduce -- do a reduction.  |
`-----------------------------*/
grecs_bind_reduce:
  /* grecs_bind_n is the number of a rule to reduce with.  */
  grecs_bind_len = grecs_bind_r2[grecs_bind_n];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  grecs_bind_val = grecs_bind_vsp[1-grecs_bind_len];

  /* Default location. */
  YYLLOC_DEFAULT (grecs_bind_loc, (grecs_bind_lsp - grecs_bind_len), grecs_bind_len);
  grecs_bind_error_range[1] = grecs_bind_loc;
  YY_REDUCE_PRINT (grecs_bind_n);
  switch (grecs_bind_n)
    {
  case 2: /* input: maybe_stmtlist  */
#line 62 "bind-gram.y"
          {
		  parse_tree = grecs_node_create(grecs_node_root, &(grecs_bind_lsp[0]));
		  parse_tree->v.texttab = grecs_text_table();
		  grecs_node_bind(parse_tree, (grecs_bind_vsp[0].node), 1);
	  }
#line 1602 "bind-gram.c"
    break;

  case 3: /* maybe_stmtlist: %empty  */
#line 71 "bind-gram.y"
          {
		  (grecs_bind_val.node) = NULL;
	  }
#line 1610 "bind-gram.c"
    break;

  case 4: /* maybe_stmtlist: stmtlist  */
#line 75 "bind-gram.y"
          {
		  (grecs_bind_val.node) = (grecs_bind_vsp[0].node_list).head;
	  }
#line 1618 "bind-gram.c"
    break;

  case 5: /* stmtlist: stmt  */
#line 81 "bind-gram.y"
          {
		  (grecs_bind_val.node_list).head = (grecs_bind_val.node_list).tail = (grecs_bind_vsp[0].node);
	  }
#line 1626 "bind-gram.c"
    break;

  case 6: /* stmtlist: stmtlist stmt  */
#line 85 "bind-gram.y"
          {
		  if ((grecs_bind_vsp[0].node)) {
			  if (!(grecs_bind_vsp[-1].node_list).head)
				  (grecs_bind_vsp[-1].node_list).head = (grecs_bind_vsp[-1].node_list).tail = (grecs_bind_vsp[0].node);
			  else
				  grecs_node_bind((grecs_bind_vsp[-1].node_list).tail, (grecs_bind_vsp[0].node), 0);
		  }
		  (grecs_bind_val.node_list) = (grecs_bind_vsp[-1].node_list);
	  }
#line 1640 "bind-gram.c"
    break;

  case 9: /* simple: BIND_IDENT vallist ';'  */
#line 101 "bind-gram.y"
          {
		  if (strcmp((grecs_bind_vsp[-2].string), "include") == 0 &&
		      (grecs_bind_vsp[-1].pvalue)->type == GRECS_TYPE_STRING) {
			  grecs_bind_new_source((grecs_bind_vsp[-1].pvalue)->v.string, &(grecs_bind_lsp[-2]));
			  (grecs_bind_val.node) = NULL;
		  } else {
			  (grecs_bind_val.node) = grecs_node_create_points(grecs_node_stmt,
							(grecs_bind_lsp[-2]).beg, (grecs_bind_lsp[-1]).end);
			  (grecs_bind_val.node)->ident = (grecs_bind_vsp[-2].string);
			  (grecs_bind_val.node)->idloc = (grecs_bind_lsp[-2]);
			  (grecs_bind_val.node)->v.value = (grecs_bind_vsp[-1].pvalue);
		  }
	  }
#line 1658 "bind-gram.c"
    break;

  case 10: /* simple: string ';'  */
#line 115 "bind-gram.y"
          {
		  (grecs_bind_val.node) = grecs_node_create(grecs_node_stmt, &(grecs_bind_lsp[-1]));
		  (grecs_bind_val.node)->ident = (grecs_bind_vsp[-1].string);
		  (grecs_bind_val.node)->idloc = (grecs_bind_lsp[-1]);
		  (grecs_bind_val.node)->v.value = NULL;
	  }
#line 1669 "bind-gram.c"
    break;

  case 11: /* simple: '!' string ';'  */
#line 122 "bind-gram.y"
          {
		  (grecs_bind_val.node) = grecs_node_create_points(grecs_node_stmt,
						(grecs_bind_lsp[-2]).beg, (grecs_bind_lsp[-1]).end);
		  (grecs_bind_val.node)->ident = grecs_strdup("!");
		  (grecs_bind_val.node)->idloc = (grecs_bind_lsp[-2]);
		  (grecs_bind_val.node)->v.value = grecs_malloc(sizeof((grecs_bind_val.node)->v.value[0]));
		  (grecs_bind_val.node)->v.value->type = GRECS_TYPE_STRING;
		  (grecs_bind_val.node)->v.value->locus = (grecs_bind_lsp[-1]);
		  (grecs_bind_val.node)->v.value->v.string = (grecs_bind_vsp[-1].string);
	  }
#line 1684 "bind-gram.c"
    break;

  case 12: /* block: BIND_IDENT tag '{' maybe_stmtlist '}' ';'  */
#line 135 "bind-gram.y"
          {
		  (grecs_bind_val.node) = grecs_node_create_points(grecs_node_block,
						(grecs_bind_lsp[-5]).beg, (grecs_bind_lsp[-1]).end);
		  (grecs_bind_val.node)->ident = (grecs_bind_vsp[-5].string);
		  (grecs_bind_val.node)->idloc = (grecs_bind_lsp[-5]);
		  (grecs_bind_val.node)->v.value = (grecs_bind_vsp[-4].pvalue);
		  grecs_node_bind((grecs_bind_val.node), (grecs_bind_vsp[-2].node), 1);
	  }
#line 1697 "bind-gram.c"
    break;

  case 13: /* block: BIND_CONTROLS '{' ctlsub '}' ';'  */
#line 156 "bind-gram.y"
          {
		  (grecs_bind_val.node) = grecs_node_create_points(grecs_node_stmt,
						(grecs_bind_lsp[-4]).beg, (grecs_bind_lsp[-1]).end);
		  (grecs_bind_val.node)->ident = (grecs_bind_vsp[-4].string);
		  (grecs_bind_val.node)->idloc = (grecs_bind_lsp[-4]);
		  (grecs_bind_val.node)->v.value = grecs_value_ptr_from_static(&(grecs_bind_vsp[-2].svalue));
	  }
#line 1709 "bind-gram.c"
    break;

  case 14: /* tag: %empty  */
#line 166 "bind-gram.y"
          {
		  (grecs_bind_val.pvalue) = NULL;
	  }
#line 1717 "bind-gram.c"
    break;

  case 16: /* vallist: vlist  */
#line 173 "bind-gram.y"
          {
		  size_t n;
		  
		  if ((n = grecs_list_size((grecs_bind_vsp[0].list))) == 1) {
			  (grecs_bind_val.pvalue) = grecs_list_index((grecs_bind_vsp[0].list), 0);
		  } else {
			  size_t i;
			  struct grecs_list_entry *ep;
		
			  (grecs_bind_val.pvalue) = grecs_malloc(sizeof((grecs_bind_val.pvalue)[0]));
			  (grecs_bind_val.pvalue)->type = GRECS_TYPE_ARRAY;
			  (grecs_bind_val.pvalue)->locus = (grecs_bind_lsp[0]);
			  (grecs_bind_val.pvalue)->v.arg.c = n;
			  (grecs_bind_val.pvalue)->v.arg.v = grecs_calloc(n,
						     sizeof((grecs_bind_val.pvalue)->v.arg.v[0]));
			  for (i = 0, ep = (grecs_bind_vsp[0].list)->head; ep; i++, ep = ep->next)
				  (grecs_bind_val.pvalue)->v.arg.v[i] = ep->data;
		  }
		  (grecs_bind_vsp[0].list)->free_entry = NULL;
		  grecs_list_free((grecs_bind_vsp[0].list));	      
	  }
#line 1743 "bind-gram.c"
    break;

  case 17: /* vlist: value  */
#line 197 "bind-gram.y"
          {
		  (grecs_bind_val.list) = grecs_value_list_create();
		  grecs_list_append((grecs_bind_val.list), grecs_value_ptr_from_static(&(grecs_bind_vsp[0].svalue)));
	  }
#line 1752 "bind-gram.c"
    break;

  case 18: /* vlist: vlist value  */
#line 202 "bind-gram.y"
          {
		  grecs_list_append((grecs_bind_vsp[-1].list), grecs_value_ptr_from_static(&(grecs_bind_vsp[0].svalue)));
	  }
#line 1760 "bind-gram.c"
    break;

  case 19: /* value: string  */
#line 208 "bind-gram.y"
          {
		  (grecs_bind_val.svalue).type = GRECS_TYPE_STRING;
		  (grecs_bind_val.svalue).locus = (grecs_bind_lsp[0]);
		  (grecs_bind_val.svalue).v.string = (grecs_bind_vsp[0].string);
	  }
#line 1770 "bind-gram.c"
    break;

  case 22: /* ctlsub: ctllist ';'  */
#line 220 "bind-gram.y"
          {
		  (grecs_bind_val.svalue).type = GRECS_TYPE_LIST;
		  (grecs_bind_val.svalue).locus = (grecs_bind_lsp[-1]);
		  (grecs_bind_val.svalue).v.list = (grecs_bind_vsp[-1].list);
	  }
#line 1780 "bind-gram.c"
    break;

  case 23: /* ctllist: value  */
#line 228 "bind-gram.y"
          {
		  (grecs_bind_val.list) = grecs_value_list_create();
		  grecs_list_append((grecs_bind_val.list), grecs_value_ptr_from_static(&(grecs_bind_vsp[0].svalue)));
	  }
#line 1789 "bind-gram.c"
    break;

  case 24: /* ctllist: ctllist value  */
#line 233 "bind-gram.y"
          {
		  grecs_list_append((grecs_bind_vsp[-1].list), grecs_value_ptr_from_static(&(grecs_bind_vsp[0].svalue)));
	  }
#line 1797 "bind-gram.c"
    break;

  case 25: /* ctllist: ctllist '{' stmtlist '}'  */
#line 237 "bind-gram.y"
          {
		  grecs_list_append((grecs_bind_vsp[-3].list), stmtlist_to_value((grecs_bind_vsp[-1].node_list).head));
		  /* FIXME: Free $3 */
	  }
#line 1806 "bind-gram.c"
    break;


#line 1810 "bind-gram.c"

      default: break;
    }
  /* User semantic actions sometimes alter grecs_bind_char, and that requires
     that grecs_bind_token be updated with the new translation.  We take the
     approach of translating immediately before every use of grecs_bind_token.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering grecs_bind_char or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (grecs_bind_symbol_kind_t, grecs_bind_r1[grecs_bind_n]), &grecs_bind_val, &grecs_bind_loc);

  YYPOPSTACK (grecs_bind_len);
  grecs_bind_len = 0;

  *++grecs_bind_vsp = grecs_bind_val;
  *++grecs_bind_lsp = grecs_bind_loc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int grecs_bind_lhs = grecs_bind_r1[grecs_bind_n] - YYNTOKENS;
    const int grecs_bind_i = grecs_bind_pgoto[grecs_bind_lhs] + *grecs_bind_ssp;
    grecs_bind_state = (0 <= grecs_bind_i && grecs_bind_i <= YYLAST && grecs_bind_check[grecs_bind_i] == *grecs_bind_ssp
               ? grecs_bind_table[grecs_bind_i]
               : grecs_bind_defgoto[grecs_bind_lhs]);
  }

  goto grecs_bind_newstate;


/*--------------------------------------.
| grecs_bind_errlab -- here on detecting error.  |
`--------------------------------------*/
grecs_bind_errlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  grecs_bind_token = grecs_bind_char == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (grecs_bind_char);
  /* If not already recovering from an error, report this error.  */
  if (!grecs_bind_errstatus)
    {
      ++grecs_bind_nerrs;
      {
        grecs_bind_pcontext_t grecs_bind_ctx
          = {grecs_bind_ssp, grecs_bind_token, &grecs_bind_lloc};
        char const *grecs_bind_msgp = YY_("syntax error");
        int grecs_bind_syntax_error_status;
        grecs_bind_syntax_error_status = grecs_bind_syntax_error (&grecs_bind_msg_alloc, &grecs_bind_msg, &grecs_bind_ctx);
        if (grecs_bind_syntax_error_status == 0)
          grecs_bind_msgp = grecs_bind_msg;
        else if (grecs_bind_syntax_error_status == -1)
          {
            if (grecs_bind_msg != grecs_bind_msgbuf)
              YYSTACK_FREE (grecs_bind_msg);
            grecs_bind_msg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, grecs_bind_msg_alloc)));
            if (grecs_bind_msg)
              {
                grecs_bind_syntax_error_status
                  = grecs_bind_syntax_error (&grecs_bind_msg_alloc, &grecs_bind_msg, &grecs_bind_ctx);
                grecs_bind_msgp = grecs_bind_msg;
              }
            else
              {
                grecs_bind_msg = grecs_bind_msgbuf;
                grecs_bind_msg_alloc = sizeof grecs_bind_msgbuf;
                grecs_bind_syntax_error_status = YYENOMEM;
              }
          }
        grecs_bind_error (grecs_bind_msgp);
        if (grecs_bind_syntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  grecs_bind_error_range[1] = grecs_bind_lloc;
  if (grecs_bind_errstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (grecs_bind_char <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (grecs_bind_char == YYEOF)
            YYABORT;
        }
      else
        {
          grecs_bind_destruct ("Error: discarding",
                      grecs_bind_token, &grecs_bind_lval, &grecs_bind_lloc);
          grecs_bind_char = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto grecs_bind_errlab1;


/*---------------------------------------------------.
| grecs_bind_errorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
grecs_bind_errorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label grecs_bind_errorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++grecs_bind_nerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (grecs_bind_len);
  grecs_bind_len = 0;
  YY_STACK_PRINT (grecs_bind_ss, grecs_bind_ssp);
  grecs_bind_state = *grecs_bind_ssp;
  goto grecs_bind_errlab1;


/*-------------------------------------------------------------.
| grecs_bind_errlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
grecs_bind_errlab1:
  grecs_bind_errstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      grecs_bind_n = grecs_bind_pact[grecs_bind_state];
      if (!grecs_bind_pact_value_is_default (grecs_bind_n))
        {
          grecs_bind_n += YYSYMBOL_YYerror;
          if (0 <= grecs_bind_n && grecs_bind_n <= YYLAST && grecs_bind_check[grecs_bind_n] == YYSYMBOL_YYerror)
            {
              grecs_bind_n = grecs_bind_table[grecs_bind_n];
              if (0 < grecs_bind_n)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (grecs_bind_ssp == grecs_bind_ss)
        YYABORT;

      grecs_bind_error_range[1] = *grecs_bind_lsp;
      grecs_bind_destruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (grecs_bind_state), grecs_bind_vsp, grecs_bind_lsp);
      YYPOPSTACK (1);
      grecs_bind_state = *grecs_bind_ssp;
      YY_STACK_PRINT (grecs_bind_ss, grecs_bind_ssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_bind_vsp = grecs_bind_lval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  grecs_bind_error_range[2] = grecs_bind_lloc;
  ++grecs_bind_lsp;
  YYLLOC_DEFAULT (*grecs_bind_lsp, grecs_bind_error_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (grecs_bind_n), grecs_bind_vsp, grecs_bind_lsp);

  grecs_bind_state = grecs_bind_n;
  goto grecs_bind_newstate;


/*-------------------------------------.
| grecs_bind_acceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
grecs_bind_acceptlab:
  grecs_bind_result = 0;
  goto grecs_bind_returnlab;


/*-----------------------------------.
| grecs_bind_abortlab -- YYABORT comes here.  |
`-----------------------------------*/
grecs_bind_abortlab:
  grecs_bind_result = 1;
  goto grecs_bind_returnlab;


/*-----------------------------------------------------------.
| grecs_bind_exhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
grecs_bind_exhaustedlab:
  grecs_bind_error (YY_("memory exhausted"));
  grecs_bind_result = 2;
  goto grecs_bind_returnlab;


/*----------------------------------------------------------.
| grecs_bind_returnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
grecs_bind_returnlab:
  if (grecs_bind_char != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      grecs_bind_token = YYTRANSLATE (grecs_bind_char);
      grecs_bind_destruct ("Cleanup: discarding lookahead",
                  grecs_bind_token, &grecs_bind_lval, &grecs_bind_lloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (grecs_bind_len);
  YY_STACK_PRINT (grecs_bind_ss, grecs_bind_ssp);
  while (grecs_bind_ssp != grecs_bind_ss)
    {
      grecs_bind_destruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*grecs_bind_ssp), grecs_bind_vsp, grecs_bind_lsp);
      YYPOPSTACK (1);
    }
#ifndef grecs_bind_overflow
  if (grecs_bind_ss != grecs_bind_ssa)
    YYSTACK_FREE (grecs_bind_ss);
#endif
  if (grecs_bind_msg != grecs_bind_msgbuf)
    YYSTACK_FREE (grecs_bind_msg);
  return grecs_bind_result;
}

#line 243 "bind-gram.y"


int
grecs_bind_error(char const *s)
{
	grecs_error(&grecs_bind_lloc, 0, "%s", s);
	return 0;
}

struct grecs_node *
grecs_bind_parser(const char *name, int traceflags)
{
	int rc;

	if (grecs_bind_new_source(name, NULL))
		return NULL;
	grecs_bind__flex_debug = traceflags & GRECS_TRACE_LEX;
	grecs_bind_debug = traceflags & GRECS_TRACE_GRAM;
	parse_tree = NULL;
	grecs_line_acc_create();
	rc = grecs_bind_parse();
	grecs_bind_close_sources();
	if (grecs_error_count)
		rc = 1;
	grecs_line_acc_free();
	if (rc) {
		grecs_tree_free(parse_tree);
		parse_tree = NULL;
	}
	return parse_tree;
}

static struct grecs_value *
node_to_value(struct grecs_node *node, struct grecs_txtacc *acc)
{
	struct grecs_value *val = grecs_malloc(sizeof(*val));
	int delim = 0;
	
	if (node->ident) {
		grecs_txtacc_grow(acc, node->ident, strlen(node->ident));
		delim = 1;
	}
	
	if (node->v.value) {
		if (delim)
			grecs_txtacc_grow_char(acc, ' ');
		grecs_txtacc_format_value(node->v.value, 0, acc);
		delim = 1;
	}
	if (node->type == grecs_node_stmt) {
		val->type = GRECS_TYPE_STRING;
		grecs_txtacc_grow_char(acc, 0);
		val->v.string = grecs_txtacc_finish(acc, 1);
	} else if (node->down) {
		struct grecs_list *list = grecs_value_list_create();
		struct grecs_node *np;

		if (delim)
			grecs_txtacc_grow_char(acc, ' ');
		for (np = node->down; np; np = np->next)
			grecs_list_append(list, node_to_value(np, acc));
		val->type = GRECS_TYPE_LIST;
		val->v.list = list;
	}
	return val;
}

static struct grecs_value *
stmtlist_to_value(struct grecs_node *node)
{
	struct grecs_txtacc *acc = grecs_txtacc_create();
	struct grecs_value *val;
	struct grecs_node parent;

	memset(&parent, 0, sizeof(parent));
	parent.type = grecs_node_block;
	parent.down = node;
	val = node_to_value(&parent, acc);
	grecs_txtacc_free(acc);
	return val;
}
