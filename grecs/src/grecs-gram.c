/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or grecs_grecs__.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with grecs_grecs_ or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "grecs-gram.y"

/* grecs - Gray's Extensible Configuration System
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Grecs is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Grecs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with Grecs. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <grecs.h>
#include <grecs-gram.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
  
int grecs_grecs_lex(void);
int grecs_grecs_error(char const *s);

static struct grecs_node *parse_tree;

#line 104 "grecs-gram.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_GRECS_GRAM_H_INCLUDED
# define YY_YY_GRECS_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int grecs_grecs_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum grecs_grecs_tokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    STRING = 258,                  /* STRING  */
    QSTRING = 259,                 /* QSTRING  */
    MSTRING = 260,                 /* MSTRING  */
    IDENT = 261                    /* IDENT  */
  };
  typedef enum grecs_grecs_tokentype grecs_grecs_token_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define STRING 258
#define QSTRING 259
#define MSTRING 260
#define IDENT 261

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 37 "grecs-gram.y"

	char *string;
	grecs_value_t svalue, *pvalue;
	struct grecs_list *list;
	struct grecs_node *node;
	grecs_locus_t locus;
	struct { struct grecs_node *head, *tail; } node_list;

#line 178 "grecs-gram.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE grecs_grecs_lval;
extern YYLTYPE grecs_grecs_lloc;

int grecs_grecs_parse (void);


#endif /* !YY_YY_GRECS_GRAM_H_INCLUDED  */
/* Symbol kind.  */
enum grecs_grecs_symbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_STRING = 3,                     /* STRING  */
  YYSYMBOL_QSTRING = 4,                    /* QSTRING  */
  YYSYMBOL_MSTRING = 5,                    /* MSTRING  */
  YYSYMBOL_IDENT = 6,                      /* IDENT  */
  YYSYMBOL_7_ = 7,                         /* ';'  */
  YYSYMBOL_8_ = 8,                         /* '{'  */
  YYSYMBOL_9_ = 9,                         /* '}'  */
  YYSYMBOL_10_ = 10,                       /* '('  */
  YYSYMBOL_11_ = 11,                       /* ')'  */
  YYSYMBOL_12_ = 12,                       /* ','  */
  YYSYMBOL_YYACCEPT = 13,                  /* $accept  */
  YYSYMBOL_input = 14,                     /* input  */
  YYSYMBOL_maybe_stmtlist = 15,            /* maybe_stmtlist  */
  YYSYMBOL_stmtlist = 16,                  /* stmtlist  */
  YYSYMBOL_stmt = 17,                      /* stmt  */
  YYSYMBOL_simple = 18,                    /* simple  */
  YYSYMBOL_block = 19,                     /* block  */
  YYSYMBOL_tag = 20,                       /* tag  */
  YYSYMBOL_vallist = 21,                   /* vallist  */
  YYSYMBOL_vlist = 22,                     /* vlist  */
  YYSYMBOL_value = 23,                     /* value  */
  YYSYMBOL_string = 24,                    /* string  */
  YYSYMBOL_slist = 25,                     /* slist  */
  YYSYMBOL_slist0 = 26,                    /* slist0  */
  YYSYMBOL_list = 27,                      /* list  */
  YYSYMBOL_values = 28,                    /* values  */
  YYSYMBOL_opt_sc = 29                     /* opt_sc  */
};
typedef enum grecs_grecs_symbol_kind_t grecs_grecs_symbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ grecs_grecs_type_int8;
#elif defined YY_STDINT_H
typedef int_least8_t grecs_grecs_type_int8;
#else
typedef signed char grecs_grecs_type_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ grecs_grecs_type_int16;
#elif defined YY_STDINT_H
typedef int_least16_t grecs_grecs_type_int16;
#else
typedef short grecs_grecs_type_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ grecs_grecs_type_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t grecs_grecs_type_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char grecs_grecs_type_uint8;
#else
typedef short grecs_grecs_type_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ grecs_grecs_type_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t grecs_grecs_type_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short grecs_grecs_type_uint16;
#else
typedef int grecs_grecs_type_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef grecs_grecs_type_int8 grecs_grecs__state_t;

/* State numbers in computations.  */
typedef int grecs_grecs__state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about grecs_grecs_lval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined grecs_grecs_overflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union grecs_grecs_alloc
{
  grecs_grecs__state_t grecs_grecs_ss_alloc;
  YYSTYPE grecs_grecs_vs_alloc;
  YYLTYPE grecs_grecs_ls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union grecs_grecs_alloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (grecs_grecs__state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T grecs_grecs_newbytes;                                         \
        YYCOPY (&grecs_grecs_ptr->Stack_alloc, Stack, grecs_grecs_size);                    \
        Stack = &grecs_grecs_ptr->Stack_alloc;                                    \
        grecs_grecs_newbytes = grecs_grecs_stacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        grecs_grecs_ptr += grecs_grecs_newbytes / YYSIZEOF (*grecs_grecs_ptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T grecs_grecs_i;                      \
          for (grecs_grecs_i = 0; grecs_grecs_i < (Count); grecs_grecs_i++)   \
            (Dst)[grecs_grecs_i] = (Src)[grecs_grecs_i];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  22
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   39

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  13
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  17
/* YYNRULES -- Number of rules.  */
#define YYNRULES  32
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  39

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   261


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_grecs_lex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (grecs_grecs_symbol_kind_t, grecs_grecs_translate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by grecs_grecs_lex.  */
static const grecs_grecs_type_int8 grecs_grecs_translate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      10,    11,     2,     2,    12,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     7,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     8,     2,     9,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const grecs_grecs_type_uint8 grecs_grecs_rline[] =
{
       0,    57,    57,    67,    70,    76,    80,    86,    87,    90,
      98,   107,   119,   122,   125,   149,   154,   160,   166,   172,
     180,   181,   182,   185,   200,   205,   212,   216,   220,   226,
     231,   238,   239
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (grecs_grecs_symbol_kind_t, grecs_grecs_stos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *grecs_grecs_symbol_name (grecs_grecs_symbol_kind_t grecs_grecs_symbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const grecs_grecs_tname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "STRING", "QSTRING",
  "MSTRING", "IDENT", "';'", "'{'", "'}'", "'('", "')'", "','", "$accept",
  "input", "maybe_stmtlist", "stmtlist", "stmt", "simple", "block", "tag",
  "vallist", "vlist", "value", "string", "slist", "slist0", "list",
  "values", "opt_sc", YY_NULLPTR
};

static const char *
grecs_grecs_symbol_name (grecs_grecs_symbol_kind_t grecs_grecs_symbol)
{
  return grecs_grecs_tname[grecs_grecs_symbol];
}
#endif

#define YYPACT_NINF (-13)

#define grecs_grecs_pact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define grecs_grecs_table_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const grecs_grecs_type_int8 grecs_grecs_pact[] =
{
       3,    20,    10,   -13,     3,   -13,   -13,   -13,   -13,   -13,
     -13,   -13,   -13,     2,    27,     4,    28,   -13,   -13,   -13,
      14,   -13,   -13,   -13,   -13,   -13,    -9,     3,   -13,   -13,
     -13,   -13,    11,    30,   -13,   -13,    12,   -13,   -13
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const grecs_grecs_type_int8 grecs_grecs_defact[] =
{
       3,    12,     0,     2,     4,     5,     7,     8,    20,    24,
      19,    21,    10,     0,     0,    13,    14,    15,    17,    22,
      23,    18,     1,     6,    26,    29,     0,     0,     9,    16,
      25,    27,     0,     0,    28,    30,    31,    32,    11
};

/* YYPGOTO[NTERM-NUM].  */
static const grecs_grecs_type_int8 grecs_grecs_pgoto[] =
{
     -13,   -13,   -13,     1,    -4,   -13,   -13,   -13,   -13,   -13,
     -12,   -13,   -13,   -13,   -13,   -13,   -13
};

/* YYDEFGOTO[NTERM-NUM].  */
static const grecs_grecs_type_int8 grecs_grecs_defgoto[] =
{
       0,     2,     3,     4,     5,     6,     7,    14,    15,    16,
      17,    18,    19,    20,    21,    26,    38
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const grecs_grecs_type_int8 grecs_grecs_table[] =
{
      23,    25,    31,    32,    29,     8,     9,    10,    11,     1,
      22,    28,    13,    24,     8,     9,    10,    11,    30,    37,
      35,    13,    34,     8,     9,    10,    11,    12,    33,    23,
      13,     8,     9,    10,    11,    27,     1,     0,    13,    36
};

static const grecs_grecs_type_int8 grecs_grecs_check[] =
{
       4,    13,    11,    12,    16,     3,     4,     5,     6,     6,
       0,     7,    10,    11,     3,     4,     5,     6,     4,     7,
      32,    10,    11,     3,     4,     5,     6,     7,    27,    33,
      10,     3,     4,     5,     6,     8,     6,    -1,    10,     9
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const grecs_grecs_type_int8 grecs_grecs_stos[] =
{
       0,     6,    14,    15,    16,    17,    18,    19,     3,     4,
       5,     6,     7,    10,    20,    21,    22,    23,    24,    25,
      26,    27,     0,    17,    11,    23,    28,     8,     7,    23,
       4,    11,    12,    16,    11,    23,     9,     7,    29
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const grecs_grecs_type_int8 grecs_grecs_r1[] =
{
       0,    13,    14,    15,    15,    16,    16,    17,    17,    18,
      18,    19,    20,    20,    21,    22,    22,    23,    23,    23,
      24,    24,    24,    25,    26,    26,    27,    27,    27,    28,
      28,    29,    29
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const grecs_grecs_type_int8 grecs_grecs_r2[] =
{
       0,     2,     1,     0,     1,     1,     2,     1,     1,     3,
       2,     6,     0,     1,     1,     1,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     2,     3,     4,     1,
       3,     0,     1
};


enum { YYENOMEM = -2 };

#define grecs_grecs_errok         (grecs_grecs_errstatus = 0)
#define grecs_grecs_clearin       (grecs_grecs_char = YYEMPTY)

#define YYACCEPT        goto grecs_grecs_acceptlab
#define YYABORT         goto grecs_grecs_abortlab
#define YYERROR         goto grecs_grecs_errorlab
#define YYNOMEM         goto grecs_grecs_exhaustedlab


#define YYRECOVERING()  (!!grecs_grecs_errstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (grecs_grecs_char == YYEMPTY)                                        \
      {                                                           \
        grecs_grecs_char = (Token);                                         \
        grecs_grecs_lval = (Value);                                         \
        YYPOPSTACK (grecs_grecs_len);                                       \
        grecs_grecs_state = *grecs_grecs_ssp;                                         \
        goto grecs_grecs_backup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        grecs_grecs_error (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (grecs_grecs_debug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
grecs_grecs__location_print_ (FILE *grecs_grecs_o, YYLTYPE const * const grecs_grecs_locp)
{
  int res = 0;
  int end_col = 0 != grecs_grecs_locp->last_column ? grecs_grecs_locp->last_column - 1 : 0;
  if (0 <= grecs_grecs_locp->first_line)
    {
      res += YYFPRINTF (grecs_grecs_o, "%d", grecs_grecs_locp->first_line);
      if (0 <= grecs_grecs_locp->first_column)
        res += YYFPRINTF (grecs_grecs_o, ".%d", grecs_grecs_locp->first_column);
    }
  if (0 <= grecs_grecs_locp->last_line)
    {
      if (grecs_grecs_locp->first_line < grecs_grecs_locp->last_line)
        {
          res += YYFPRINTF (grecs_grecs_o, "-%d", grecs_grecs_locp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (grecs_grecs_o, ".%d", end_col);
        }
      else if (0 <= end_col && grecs_grecs_locp->first_column < end_col)
        res += YYFPRINTF (grecs_grecs_o, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  grecs_grecs__location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (grecs_grecs_debug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      grecs_grecs__symbol_print (stderr,                                            \
                  Kind, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
grecs_grecs__symbol_value_print (FILE *grecs_grecs_o,
                       grecs_grecs_symbol_kind_t grecs_grecs_kind, YYSTYPE const * const grecs_grecs_valuep, YYLTYPE const * const grecs_grecs_locationp)
{
  FILE *grecs_grecs_output = grecs_grecs_o;
  YY_USE (grecs_grecs_output);
  YY_USE (grecs_grecs_locationp);
  if (!grecs_grecs_valuep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_grecs_kind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
grecs_grecs__symbol_print (FILE *grecs_grecs_o,
                 grecs_grecs_symbol_kind_t grecs_grecs_kind, YYSTYPE const * const grecs_grecs_valuep, YYLTYPE const * const grecs_grecs_locationp)
{
  YYFPRINTF (grecs_grecs_o, "%s %s (",
             grecs_grecs_kind < YYNTOKENS ? "token" : "nterm", grecs_grecs_symbol_name (grecs_grecs_kind));

  YYLOCATION_PRINT (grecs_grecs_o, grecs_grecs_locationp);
  YYFPRINTF (grecs_grecs_o, ": ");
  grecs_grecs__symbol_value_print (grecs_grecs_o, grecs_grecs_kind, grecs_grecs_valuep, grecs_grecs_locationp);
  YYFPRINTF (grecs_grecs_o, ")");
}

/*------------------------------------------------------------------.
| grecs_grecs__stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
grecs_grecs__stack_print (grecs_grecs__state_t *grecs_grecs_bottom, grecs_grecs__state_t *grecs_grecs_top)
{
  YYFPRINTF (stderr, "Stack now");
  for (; grecs_grecs_bottom <= grecs_grecs_top; grecs_grecs_bottom++)
    {
      int grecs_grecs_bot = *grecs_grecs_bottom;
      YYFPRINTF (stderr, " %d", grecs_grecs_bot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (grecs_grecs_debug)                                                  \
    grecs_grecs__stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
grecs_grecs__reduce_print (grecs_grecs__state_t *grecs_grecs_ssp, YYSTYPE *grecs_grecs_vsp, YYLTYPE *grecs_grecs_lsp,
                 int grecs_grecs_rule)
{
  int grecs_grecs_lno = grecs_grecs_rline[grecs_grecs_rule];
  int grecs_grecs_nrhs = grecs_grecs_r2[grecs_grecs_rule];
  int grecs_grecs_i;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             grecs_grecs_rule - 1, grecs_grecs_lno);
  /* The symbols being reduced.  */
  for (grecs_grecs_i = 0; grecs_grecs_i < grecs_grecs_nrhs; grecs_grecs_i++)
    {
      YYFPRINTF (stderr, "   $%d = ", grecs_grecs_i + 1);
      grecs_grecs__symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+grecs_grecs_ssp[grecs_grecs_i + 1 - grecs_grecs_nrhs]),
                       &grecs_grecs_vsp[(grecs_grecs_i + 1) - (grecs_grecs_nrhs)],
                       &(grecs_grecs_lsp[(grecs_grecs_i + 1) - (grecs_grecs_nrhs)]));
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (grecs_grecs_debug)                          \
    grecs_grecs__reduce_print (grecs_grecs_ssp, grecs_grecs_vsp, grecs_grecs_lsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int grecs_grecs_debug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  grecs_grecs__state_t *grecs_grecs_ssp;
  grecs_grecs_symbol_kind_t grecs_grecs_token;
  YYLTYPE *grecs_grecs_lloc;
} grecs_grecs_pcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
grecs_grecs_pcontext_expected_tokens (const grecs_grecs_pcontext_t *grecs_grecs_ctx,
                            grecs_grecs_symbol_kind_t grecs_grecs_arg[], int grecs_grecs_argn)
{
  /* Actual size of YYARG. */
  int grecs_grecs_count = 0;
  int grecs_grecs_n = grecs_grecs_pact[+*grecs_grecs_ctx->grecs_grecs_ssp];
  if (!grecs_grecs_pact_value_is_default (grecs_grecs_n))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int grecs_grecs_xbegin = grecs_grecs_n < 0 ? -grecs_grecs_n : 0;
      /* Stay within bounds of both grecs_grecs_check and grecs_grecs_tname.  */
      int grecs_grecs_checklim = YYLAST - grecs_grecs_n + 1;
      int grecs_grecs_xend = grecs_grecs_checklim < YYNTOKENS ? grecs_grecs_checklim : YYNTOKENS;
      int grecs_grecs_x;
      for (grecs_grecs_x = grecs_grecs_xbegin; grecs_grecs_x < grecs_grecs_xend; ++grecs_grecs_x)
        if (grecs_grecs_check[grecs_grecs_x + grecs_grecs_n] == grecs_grecs_x && grecs_grecs_x != YYSYMBOL_YYerror
            && !grecs_grecs_table_value_is_error (grecs_grecs_table[grecs_grecs_x + grecs_grecs_n]))
          {
            if (!grecs_grecs_arg)
              ++grecs_grecs_count;
            else if (grecs_grecs_count == grecs_grecs_argn)
              return 0;
            else
              grecs_grecs_arg[grecs_grecs_count++] = YY_CAST (grecs_grecs_symbol_kind_t, grecs_grecs_x);
          }
    }
  if (grecs_grecs_arg && grecs_grecs_count == 0 && 0 < grecs_grecs_argn)
    grecs_grecs_arg[0] = YYSYMBOL_YYEMPTY;
  return grecs_grecs_count;
}




#ifndef grecs_grecs_strlen
# if defined __GLIBC__ && defined _STRING_H
#  define grecs_grecs_strlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
grecs_grecs_strlen (const char *grecs_grecs_str)
{
  YYPTRDIFF_T grecs_grecs_len;
  for (grecs_grecs_len = 0; grecs_grecs_str[grecs_grecs_len]; grecs_grecs_len++)
    continue;
  return grecs_grecs_len;
}
# endif
#endif

#ifndef grecs_grecs_stpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define grecs_grecs_stpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
grecs_grecs_stpcpy (char *grecs_grecs_dest, const char *grecs_grecs_src)
{
  char *grecs_grecs_d = grecs_grecs_dest;
  const char *grecs_grecs_s = grecs_grecs_src;

  while ((*grecs_grecs_d++ = *grecs_grecs_s++) != '\0')
    continue;

  return grecs_grecs_d - 1;
}
# endif
#endif

#ifndef grecs_grecs_tnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for grecs_grecs_error.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from grecs_grecs_tname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
grecs_grecs_tnamerr (char *grecs_grecs_res, const char *grecs_grecs_str)
{
  if (*grecs_grecs_str == '"')
    {
      YYPTRDIFF_T grecs_grecs_n = 0;
      char const *grecs_grecs_p = grecs_grecs_str;
      for (;;)
        switch (*++grecs_grecs_p)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++grecs_grecs_p != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (grecs_grecs_res)
              grecs_grecs_res[grecs_grecs_n] = *grecs_grecs_p;
            grecs_grecs_n++;
            break;

          case '"':
            if (grecs_grecs_res)
              grecs_grecs_res[grecs_grecs_n] = '\0';
            return grecs_grecs_n;
          }
    do_not_strip_quotes: ;
    }

  if (grecs_grecs_res)
    return grecs_grecs_stpcpy (grecs_grecs_res, grecs_grecs_str) - grecs_grecs_res;
  else
    return grecs_grecs_strlen (grecs_grecs_str);
}
#endif


static int
grecs_grecs__syntax_error_arguments (const grecs_grecs_pcontext_t *grecs_grecs_ctx,
                           grecs_grecs_symbol_kind_t grecs_grecs_arg[], int grecs_grecs_argn)
{
  /* Actual size of YYARG. */
  int grecs_grecs_count = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in grecs_grecs_char) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated grecs_grecs_char.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (grecs_grecs_ctx->grecs_grecs_token != YYSYMBOL_YYEMPTY)
    {
      int grecs_grecs_n;
      if (grecs_grecs_arg)
        grecs_grecs_arg[grecs_grecs_count] = grecs_grecs_ctx->grecs_grecs_token;
      ++grecs_grecs_count;
      grecs_grecs_n = grecs_grecs_pcontext_expected_tokens (grecs_grecs_ctx,
                                        grecs_grecs_arg ? grecs_grecs_arg + 1 : grecs_grecs_arg, grecs_grecs_argn - 1);
      if (grecs_grecs_n == YYENOMEM)
        return YYENOMEM;
      else
        grecs_grecs_count += grecs_grecs_n;
    }
  return grecs_grecs_count;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
grecs_grecs_syntax_error (YYPTRDIFF_T *grecs_grecs_msg_alloc, char **grecs_grecs_msg,
                const grecs_grecs_pcontext_t *grecs_grecs_ctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *grecs_grecs_format = YY_NULLPTR;
  /* Arguments of grecs_grecs_format: reported tokens (one for the "unexpected",
     one per "expected"). */
  grecs_grecs_symbol_kind_t grecs_grecs_arg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T grecs_grecs_size = 0;

  /* Actual size of YYARG. */
  int grecs_grecs_count = grecs_grecs__syntax_error_arguments (grecs_grecs_ctx, grecs_grecs_arg, YYARGS_MAX);
  if (grecs_grecs_count == YYENOMEM)
    return YYENOMEM;

  switch (grecs_grecs_count)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        grecs_grecs_format = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  grecs_grecs_size = grecs_grecs_strlen (grecs_grecs_format) - 2 * grecs_grecs_count + 1;
  {
    int grecs_grecs_i;
    for (grecs_grecs_i = 0; grecs_grecs_i < grecs_grecs_count; ++grecs_grecs_i)
      {
        YYPTRDIFF_T grecs_grecs_size1
          = grecs_grecs_size + grecs_grecs_tnamerr (YY_NULLPTR, grecs_grecs_tname[grecs_grecs_arg[grecs_grecs_i]]);
        if (grecs_grecs_size <= grecs_grecs_size1 && grecs_grecs_size1 <= YYSTACK_ALLOC_MAXIMUM)
          grecs_grecs_size = grecs_grecs_size1;
        else
          return YYENOMEM;
      }
  }

  if (*grecs_grecs_msg_alloc < grecs_grecs_size)
    {
      *grecs_grecs_msg_alloc = 2 * grecs_grecs_size;
      if (! (grecs_grecs_size <= *grecs_grecs_msg_alloc
             && *grecs_grecs_msg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *grecs_grecs_msg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *grecs_grecs_p = *grecs_grecs_msg;
    int grecs_grecs_i = 0;
    while ((*grecs_grecs_p = *grecs_grecs_format) != '\0')
      if (*grecs_grecs_p == '%' && grecs_grecs_format[1] == 's' && grecs_grecs_i < grecs_grecs_count)
        {
          grecs_grecs_p += grecs_grecs_tnamerr (grecs_grecs_p, grecs_grecs_tname[grecs_grecs_arg[grecs_grecs_i++]]);
          grecs_grecs_format += 2;
        }
      else
        {
          ++grecs_grecs_p;
          ++grecs_grecs_format;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
grecs_grecs_destruct (const char *grecs_grecs_msg,
            grecs_grecs_symbol_kind_t grecs_grecs_kind, YYSTYPE *grecs_grecs_valuep, YYLTYPE *grecs_grecs_locationp)
{
  YY_USE (grecs_grecs_valuep);
  YY_USE (grecs_grecs_locationp);
  if (!grecs_grecs_msg)
    grecs_grecs_msg = "Deleting";
  YY_SYMBOL_PRINT (grecs_grecs_msg, grecs_grecs_kind, grecs_grecs_valuep, grecs_grecs_locationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (grecs_grecs_kind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int grecs_grecs_char;

/* The semantic value of the lookahead symbol.  */
YYSTYPE grecs_grecs_lval;
/* Location data for the lookahead symbol.  */
YYLTYPE grecs_grecs_lloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int grecs_grecs_nerrs;




/*----------.
| grecs_grecs_parse.  |
`----------*/

int
grecs_grecs_parse (void)
{
    grecs_grecs__state_fast_t grecs_grecs_state = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int grecs_grecs_errstatus = 0;

    /* Refer to the stacks through separate pointers, to allow grecs_grecs_overflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T grecs_grecs_stacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    grecs_grecs__state_t grecs_grecs_ssa[YYINITDEPTH];
    grecs_grecs__state_t *grecs_grecs_ss = grecs_grecs_ssa;
    grecs_grecs__state_t *grecs_grecs_ssp = grecs_grecs_ss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE grecs_grecs_vsa[YYINITDEPTH];
    YYSTYPE *grecs_grecs_vs = grecs_grecs_vsa;
    YYSTYPE *grecs_grecs_vsp = grecs_grecs_vs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE grecs_grecs_lsa[YYINITDEPTH];
    YYLTYPE *grecs_grecs_ls = grecs_grecs_lsa;
    YYLTYPE *grecs_grecs_lsp = grecs_grecs_ls;

  int grecs_grecs_n;
  /* The return value of grecs_grecs_parse.  */
  int grecs_grecs_result;
  /* Lookahead symbol kind.  */
  grecs_grecs_symbol_kind_t grecs_grecs_token = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE grecs_grecs_val;
  YYLTYPE grecs_grecs_loc;

  /* The locations where the error started and ended.  */
  YYLTYPE grecs_grecs_error_range[3];

  /* Buffer for error messages, and its allocated size.  */
  char grecs_grecs_msgbuf[128];
  char *grecs_grecs_msg = grecs_grecs_msgbuf;
  YYPTRDIFF_T grecs_grecs_msg_alloc = sizeof grecs_grecs_msgbuf;

#define YYPOPSTACK(N)   (grecs_grecs_vsp -= (N), grecs_grecs_ssp -= (N), grecs_grecs_lsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int grecs_grecs_len = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  grecs_grecs_char = YYEMPTY; /* Cause a token to be read.  */

  grecs_grecs_lsp[0] = grecs_grecs_lloc;
  goto grecs_grecs_setstate;


/*------------------------------------------------------------.
| grecs_grecs_newstate -- push a new state, which is found in grecs_grecs_state.  |
`------------------------------------------------------------*/
grecs_grecs_newstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  grecs_grecs_ssp++;


/*--------------------------------------------------------------------.
| grecs_grecs_setstate -- set current state (the top of the stack) to grecs_grecs_state.  |
`--------------------------------------------------------------------*/
grecs_grecs_setstate:
  YYDPRINTF ((stderr, "Entering state %d\n", grecs_grecs_state));
  YY_ASSERT (0 <= grecs_grecs_state && grecs_grecs_state < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *grecs_grecs_ssp = YY_CAST (grecs_grecs__state_t, grecs_grecs_state);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (grecs_grecs_ss, grecs_grecs_ssp);

  if (grecs_grecs_ss + grecs_grecs_stacksize - 1 <= grecs_grecs_ssp)
#if !defined grecs_grecs_overflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T grecs_grecs_size = grecs_grecs_ssp - grecs_grecs_ss + 1;

# if defined grecs_grecs_overflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        grecs_grecs__state_t *grecs_grecs_ss1 = grecs_grecs_ss;
        YYSTYPE *grecs_grecs_vs1 = grecs_grecs_vs;
        YYLTYPE *grecs_grecs_ls1 = grecs_grecs_ls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if grecs_grecs_overflow is a macro.  */
        grecs_grecs_overflow (YY_("memory exhausted"),
                    &grecs_grecs_ss1, grecs_grecs_size * YYSIZEOF (*grecs_grecs_ssp),
                    &grecs_grecs_vs1, grecs_grecs_size * YYSIZEOF (*grecs_grecs_vsp),
                    &grecs_grecs_ls1, grecs_grecs_size * YYSIZEOF (*grecs_grecs_lsp),
                    &grecs_grecs_stacksize);
        grecs_grecs_ss = grecs_grecs_ss1;
        grecs_grecs_vs = grecs_grecs_vs1;
        grecs_grecs_ls = grecs_grecs_ls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= grecs_grecs_stacksize)
        YYNOMEM;
      grecs_grecs_stacksize *= 2;
      if (YYMAXDEPTH < grecs_grecs_stacksize)
        grecs_grecs_stacksize = YYMAXDEPTH;

      {
        grecs_grecs__state_t *grecs_grecs_ss1 = grecs_grecs_ss;
        union grecs_grecs_alloc *grecs_grecs_ptr =
          YY_CAST (union grecs_grecs_alloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (grecs_grecs_stacksize))));
        if (! grecs_grecs_ptr)
          YYNOMEM;
        YYSTACK_RELOCATE (grecs_grecs_ss_alloc, grecs_grecs_ss);
        YYSTACK_RELOCATE (grecs_grecs_vs_alloc, grecs_grecs_vs);
        YYSTACK_RELOCATE (grecs_grecs_ls_alloc, grecs_grecs_ls);
#  undef YYSTACK_RELOCATE
        if (grecs_grecs_ss1 != grecs_grecs_ssa)
          YYSTACK_FREE (grecs_grecs_ss1);
      }
# endif

      grecs_grecs_ssp = grecs_grecs_ss + grecs_grecs_size - 1;
      grecs_grecs_vsp = grecs_grecs_vs + grecs_grecs_size - 1;
      grecs_grecs_lsp = grecs_grecs_ls + grecs_grecs_size - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, grecs_grecs_stacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (grecs_grecs_ss + grecs_grecs_stacksize - 1 <= grecs_grecs_ssp)
        YYABORT;
    }
#endif /* !defined grecs_grecs_overflow && !defined YYSTACK_RELOCATE */


  if (grecs_grecs_state == YYFINAL)
    YYACCEPT;

  goto grecs_grecs_backup;


/*-----------.
| grecs_grecs_backup.  |
`-----------*/
grecs_grecs_backup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  grecs_grecs_n = grecs_grecs_pact[grecs_grecs_state];
  if (grecs_grecs_pact_value_is_default (grecs_grecs_n))
    goto grecs_grecs_default;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (grecs_grecs_char == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      grecs_grecs_char = grecs_grecs_lex ();
    }

  if (grecs_grecs_char <= YYEOF)
    {
      grecs_grecs_char = YYEOF;
      grecs_grecs_token = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (grecs_grecs_char == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      grecs_grecs_char = YYUNDEF;
      grecs_grecs_token = YYSYMBOL_YYerror;
      grecs_grecs_error_range[1] = grecs_grecs_lloc;
      goto grecs_grecs_errlab1;
    }
  else
    {
      grecs_grecs_token = YYTRANSLATE (grecs_grecs_char);
      YY_SYMBOL_PRINT ("Next token is", grecs_grecs_token, &grecs_grecs_lval, &grecs_grecs_lloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  grecs_grecs_n += grecs_grecs_token;
  if (grecs_grecs_n < 0 || YYLAST < grecs_grecs_n || grecs_grecs_check[grecs_grecs_n] != grecs_grecs_token)
    goto grecs_grecs_default;
  grecs_grecs_n = grecs_grecs_table[grecs_grecs_n];
  if (grecs_grecs_n <= 0)
    {
      if (grecs_grecs_table_value_is_error (grecs_grecs_n))
        goto grecs_grecs_errlab;
      grecs_grecs_n = -grecs_grecs_n;
      goto grecs_grecs_reduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (grecs_grecs_errstatus)
    grecs_grecs_errstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", grecs_grecs_token, &grecs_grecs_lval, &grecs_grecs_lloc);
  grecs_grecs_state = grecs_grecs_n;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_grecs_vsp = grecs_grecs_lval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++grecs_grecs_lsp = grecs_grecs_lloc;

  /* Discard the shifted token.  */
  grecs_grecs_char = YYEMPTY;
  goto grecs_grecs_newstate;


/*-----------------------------------------------------------.
| grecs_grecs_default -- do the default action for the current state.  |
`-----------------------------------------------------------*/
grecs_grecs_default:
  grecs_grecs_n = grecs_grecs_defact[grecs_grecs_state];
  if (grecs_grecs_n == 0)
    goto grecs_grecs_errlab;
  goto grecs_grecs_reduce;


/*-----------------------------.
| grecs_grecs_reduce -- do a reduction.  |
`-----------------------------*/
grecs_grecs_reduce:
  /* grecs_grecs_n is the number of a rule to reduce with.  */
  grecs_grecs_len = grecs_grecs_r2[grecs_grecs_n];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  grecs_grecs_val = grecs_grecs_vsp[1-grecs_grecs_len];

  /* Default location. */
  YYLLOC_DEFAULT (grecs_grecs_loc, (grecs_grecs_lsp - grecs_grecs_len), grecs_grecs_len);
  grecs_grecs_error_range[1] = grecs_grecs_loc;
  YY_REDUCE_PRINT (grecs_grecs_n);
  switch (grecs_grecs_n)
    {
  case 2: /* input: maybe_stmtlist  */
#line 58 "grecs-gram.y"
          {
		  parse_tree = grecs_node_create(grecs_node_root, &(grecs_grecs_lsp[0]));
		  parse_tree->v.texttab = grecs_text_table();
		  grecs_node_bind(parse_tree, (grecs_grecs_vsp[0].node), 1);
	  }
#line 1607 "grecs-gram.c"
    break;

  case 3: /* maybe_stmtlist: %empty  */
#line 67 "grecs-gram.y"
          {
		  (grecs_grecs_val.node) = NULL;
	  }
#line 1615 "grecs-gram.c"
    break;

  case 4: /* maybe_stmtlist: stmtlist  */
#line 71 "grecs-gram.y"
          {
		  (grecs_grecs_val.node) = (grecs_grecs_vsp[0].node_list).head;
	  }
#line 1623 "grecs-gram.c"
    break;

  case 5: /* stmtlist: stmt  */
#line 77 "grecs-gram.y"
          {
		  (grecs_grecs_val.node_list).head = (grecs_grecs_val.node_list).tail = (grecs_grecs_vsp[0].node);
	  }
#line 1631 "grecs-gram.c"
    break;

  case 6: /* stmtlist: stmtlist stmt  */
#line 81 "grecs-gram.y"
          {
		  grecs_node_bind((grecs_grecs_vsp[-1].node_list).tail, (grecs_grecs_vsp[0].node), 0);
	  }
#line 1639 "grecs-gram.c"
    break;

  case 9: /* simple: IDENT vallist ';'  */
#line 91 "grecs-gram.y"
          {
		  (grecs_grecs_val.node) = grecs_node_create_points(grecs_node_stmt,
						(grecs_grecs_lsp[-2]).beg, (grecs_grecs_lsp[-1]).end);
		  (grecs_grecs_val.node)->ident = (grecs_grecs_vsp[-2].string);
		  (grecs_grecs_val.node)->idloc = (grecs_grecs_lsp[-2]);
		  (grecs_grecs_val.node)->v.value = (grecs_grecs_vsp[-1].pvalue);
	  }
#line 1651 "grecs-gram.c"
    break;

  case 10: /* simple: IDENT ';'  */
#line 99 "grecs-gram.y"
          {
		  (grecs_grecs_val.node) = grecs_node_create(grecs_node_stmt, &(grecs_grecs_lsp[-1]));
		  (grecs_grecs_val.node)->ident = (grecs_grecs_vsp[-1].string);
		  (grecs_grecs_val.node)->idloc = (grecs_grecs_lsp[-1]);
		  (grecs_grecs_val.node)->v.value = NULL;
	  }
#line 1662 "grecs-gram.c"
    break;

  case 11: /* block: IDENT tag '{' stmtlist '}' opt_sc  */
#line 108 "grecs-gram.y"
          {
		  (grecs_grecs_val.node) = grecs_node_create_points(grecs_node_block,
						(grecs_grecs_lsp[-5]).beg, (grecs_grecs_lsp[-1]).end);
		  (grecs_grecs_val.node)->ident = (grecs_grecs_vsp[-5].string);
		  (grecs_grecs_val.node)->idloc = (grecs_grecs_lsp[-5]);
		  (grecs_grecs_val.node)->v.value = (grecs_grecs_vsp[-4].pvalue);
		  grecs_node_bind((grecs_grecs_val.node), (grecs_grecs_vsp[-2].node_list).head, 1);
	  }
#line 1675 "grecs-gram.c"
    break;

  case 12: /* tag: %empty  */
#line 119 "grecs-gram.y"
          {
		  (grecs_grecs_val.pvalue) = NULL;
	  }
#line 1683 "grecs-gram.c"
    break;

  case 14: /* vallist: vlist  */
#line 126 "grecs-gram.y"
          {
		  size_t n;
		  
		  if ((n = grecs_list_size((grecs_grecs_vsp[0].list))) == 1) {
			  (grecs_grecs_val.pvalue) = grecs_list_index((grecs_grecs_vsp[0].list), 0);
		  } else {
			  size_t i;
			  struct grecs_list_entry *ep;
		
			  (grecs_grecs_val.pvalue) = grecs_malloc(sizeof((grecs_grecs_val.pvalue)[0]));
			  (grecs_grecs_val.pvalue)->type = GRECS_TYPE_ARRAY;
			  (grecs_grecs_val.pvalue)->locus = (grecs_grecs_lsp[0]);
			  (grecs_grecs_val.pvalue)->v.arg.c = n;
			  (grecs_grecs_val.pvalue)->v.arg.v = grecs_calloc(n,
						     sizeof((grecs_grecs_val.pvalue)->v.arg.v[0]));
			  for (i = 0, ep = (grecs_grecs_vsp[0].list)->head; ep; i++, ep = ep->next)
				  (grecs_grecs_val.pvalue)->v.arg.v[i] = ep->data;
		  }
		  (grecs_grecs_vsp[0].list)->free_entry = NULL;
		  grecs_list_free((grecs_grecs_vsp[0].list));	      
	  }
#line 1709 "grecs-gram.c"
    break;

  case 15: /* vlist: value  */
#line 150 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = grecs_value_list_create();
		  grecs_list_append((grecs_grecs_val.list), grecs_value_ptr_from_static(&(grecs_grecs_vsp[0].svalue)));
	  }
#line 1718 "grecs-gram.c"
    break;

  case 16: /* vlist: vlist value  */
#line 155 "grecs-gram.y"
          {
		  grecs_list_append((grecs_grecs_vsp[-1].list), grecs_value_ptr_from_static(&(grecs_grecs_vsp[0].svalue)));
	  }
#line 1726 "grecs-gram.c"
    break;

  case 17: /* value: string  */
#line 161 "grecs-gram.y"
          {
		  (grecs_grecs_val.svalue).type = GRECS_TYPE_STRING;
		  (grecs_grecs_val.svalue).locus = (grecs_grecs_lsp[0]);
		  (grecs_grecs_val.svalue).v.string = (grecs_grecs_vsp[0].string);
	  }
#line 1736 "grecs-gram.c"
    break;

  case 18: /* value: list  */
#line 167 "grecs-gram.y"
          {
		  (grecs_grecs_val.svalue).type = GRECS_TYPE_LIST;
		  (grecs_grecs_val.svalue).locus = (grecs_grecs_lsp[0]);
		  (grecs_grecs_val.svalue).v.list = (grecs_grecs_vsp[0].list);
	  }
#line 1746 "grecs-gram.c"
    break;

  case 19: /* value: MSTRING  */
#line 173 "grecs-gram.y"
          {
		  (grecs_grecs_val.svalue).type = GRECS_TYPE_STRING;
		  (grecs_grecs_val.svalue).locus = (grecs_grecs_lsp[0]);
		  (grecs_grecs_val.svalue).v.string = (grecs_grecs_vsp[0].string);
	  }
#line 1756 "grecs-gram.c"
    break;

  case 23: /* slist: slist0  */
#line 186 "grecs-gram.y"
          {
		  struct grecs_list_entry *ep;
		  
		  grecs_line_begin();
		  for (ep = (grecs_grecs_vsp[0].list)->head; ep; ep = ep->next) {
			  grecs_line_add(ep->data, strlen(ep->data));
			  free(ep->data);
			  ep->data = NULL;
		  }
		  (grecs_grecs_val.string) = grecs_line_finish();
		  grecs_list_free((grecs_grecs_vsp[0].list));
	  }
#line 1773 "grecs-gram.c"
    break;

  case 24: /* slist0: QSTRING  */
#line 201 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = grecs_list_create();
		  grecs_list_append((grecs_grecs_val.list), (grecs_grecs_vsp[0].string));
	  }
#line 1782 "grecs-gram.c"
    break;

  case 25: /* slist0: slist0 QSTRING  */
#line 206 "grecs-gram.y"
          {
		  grecs_list_append((grecs_grecs_vsp[-1].list), (grecs_grecs_vsp[0].string));
		  (grecs_grecs_val.list) = (grecs_grecs_vsp[-1].list);
	  }
#line 1791 "grecs-gram.c"
    break;

  case 26: /* list: '(' ')'  */
#line 213 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = NULL;
	  }
#line 1799 "grecs-gram.c"
    break;

  case 27: /* list: '(' values ')'  */
#line 217 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = (grecs_grecs_vsp[-1].list);
	  }
#line 1807 "grecs-gram.c"
    break;

  case 28: /* list: '(' values ',' ')'  */
#line 221 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = (grecs_grecs_vsp[-2].list);
	  }
#line 1815 "grecs-gram.c"
    break;

  case 29: /* values: value  */
#line 227 "grecs-gram.y"
          {
		  (grecs_grecs_val.list) = grecs_value_list_create();
		  grecs_list_append((grecs_grecs_val.list), grecs_value_ptr_from_static(&(grecs_grecs_vsp[0].svalue)));
	  }
#line 1824 "grecs-gram.c"
    break;

  case 30: /* values: values ',' value  */
#line 232 "grecs-gram.y"
          {
		  grecs_list_append((grecs_grecs_vsp[-2].list), grecs_value_ptr_from_static(&(grecs_grecs_vsp[0].svalue)));
		  (grecs_grecs_val.list) = (grecs_grecs_vsp[-2].list);
	  }
#line 1833 "grecs-gram.c"
    break;


#line 1837 "grecs-gram.c"

      default: break;
    }
  /* User semantic actions sometimes alter grecs_grecs_char, and that requires
     that grecs_grecs_token be updated with the new translation.  We take the
     approach of translating immediately before every use of grecs_grecs_token.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering grecs_grecs_char or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (grecs_grecs_symbol_kind_t, grecs_grecs_r1[grecs_grecs_n]), &grecs_grecs_val, &grecs_grecs_loc);

  YYPOPSTACK (grecs_grecs_len);
  grecs_grecs_len = 0;

  *++grecs_grecs_vsp = grecs_grecs_val;
  *++grecs_grecs_lsp = grecs_grecs_loc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int grecs_grecs_lhs = grecs_grecs_r1[grecs_grecs_n] - YYNTOKENS;
    const int grecs_grecs_i = grecs_grecs_pgoto[grecs_grecs_lhs] + *grecs_grecs_ssp;
    grecs_grecs_state = (0 <= grecs_grecs_i && grecs_grecs_i <= YYLAST && grecs_grecs_check[grecs_grecs_i] == *grecs_grecs_ssp
               ? grecs_grecs_table[grecs_grecs_i]
               : grecs_grecs_defgoto[grecs_grecs_lhs]);
  }

  goto grecs_grecs_newstate;


/*--------------------------------------.
| grecs_grecs_errlab -- here on detecting error.  |
`--------------------------------------*/
grecs_grecs_errlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  grecs_grecs_token = grecs_grecs_char == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (grecs_grecs_char);
  /* If not already recovering from an error, report this error.  */
  if (!grecs_grecs_errstatus)
    {
      ++grecs_grecs_nerrs;
      {
        grecs_grecs_pcontext_t grecs_grecs_ctx
          = {grecs_grecs_ssp, grecs_grecs_token, &grecs_grecs_lloc};
        char const *grecs_grecs_msgp = YY_("syntax error");
        int grecs_grecs_syntax_error_status;
        grecs_grecs_syntax_error_status = grecs_grecs_syntax_error (&grecs_grecs_msg_alloc, &grecs_grecs_msg, &grecs_grecs_ctx);
        if (grecs_grecs_syntax_error_status == 0)
          grecs_grecs_msgp = grecs_grecs_msg;
        else if (grecs_grecs_syntax_error_status == -1)
          {
            if (grecs_grecs_msg != grecs_grecs_msgbuf)
              YYSTACK_FREE (grecs_grecs_msg);
            grecs_grecs_msg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, grecs_grecs_msg_alloc)));
            if (grecs_grecs_msg)
              {
                grecs_grecs_syntax_error_status
                  = grecs_grecs_syntax_error (&grecs_grecs_msg_alloc, &grecs_grecs_msg, &grecs_grecs_ctx);
                grecs_grecs_msgp = grecs_grecs_msg;
              }
            else
              {
                grecs_grecs_msg = grecs_grecs_msgbuf;
                grecs_grecs_msg_alloc = sizeof grecs_grecs_msgbuf;
                grecs_grecs_syntax_error_status = YYENOMEM;
              }
          }
        grecs_grecs_error (grecs_grecs_msgp);
        if (grecs_grecs_syntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  grecs_grecs_error_range[1] = grecs_grecs_lloc;
  if (grecs_grecs_errstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (grecs_grecs_char <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (grecs_grecs_char == YYEOF)
            YYABORT;
        }
      else
        {
          grecs_grecs_destruct ("Error: discarding",
                      grecs_grecs_token, &grecs_grecs_lval, &grecs_grecs_lloc);
          grecs_grecs_char = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto grecs_grecs_errlab1;


/*---------------------------------------------------.
| grecs_grecs_errorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
grecs_grecs_errorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label grecs_grecs_errorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++grecs_grecs_nerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (grecs_grecs_len);
  grecs_grecs_len = 0;
  YY_STACK_PRINT (grecs_grecs_ss, grecs_grecs_ssp);
  grecs_grecs_state = *grecs_grecs_ssp;
  goto grecs_grecs_errlab1;


/*-------------------------------------------------------------.
| grecs_grecs_errlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
grecs_grecs_errlab1:
  grecs_grecs_errstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      grecs_grecs_n = grecs_grecs_pact[grecs_grecs_state];
      if (!grecs_grecs_pact_value_is_default (grecs_grecs_n))
        {
          grecs_grecs_n += YYSYMBOL_YYerror;
          if (0 <= grecs_grecs_n && grecs_grecs_n <= YYLAST && grecs_grecs_check[grecs_grecs_n] == YYSYMBOL_YYerror)
            {
              grecs_grecs_n = grecs_grecs_table[grecs_grecs_n];
              if (0 < grecs_grecs_n)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (grecs_grecs_ssp == grecs_grecs_ss)
        YYABORT;

      grecs_grecs_error_range[1] = *grecs_grecs_lsp;
      grecs_grecs_destruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (grecs_grecs_state), grecs_grecs_vsp, grecs_grecs_lsp);
      YYPOPSTACK (1);
      grecs_grecs_state = *grecs_grecs_ssp;
      YY_STACK_PRINT (grecs_grecs_ss, grecs_grecs_ssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++grecs_grecs_vsp = grecs_grecs_lval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  grecs_grecs_error_range[2] = grecs_grecs_lloc;
  ++grecs_grecs_lsp;
  YYLLOC_DEFAULT (*grecs_grecs_lsp, grecs_grecs_error_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (grecs_grecs_n), grecs_grecs_vsp, grecs_grecs_lsp);

  grecs_grecs_state = grecs_grecs_n;
  goto grecs_grecs_newstate;


/*-------------------------------------.
| grecs_grecs_acceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
grecs_grecs_acceptlab:
  grecs_grecs_result = 0;
  goto grecs_grecs_returnlab;


/*-----------------------------------.
| grecs_grecs_abortlab -- YYABORT comes here.  |
`-----------------------------------*/
grecs_grecs_abortlab:
  grecs_grecs_result = 1;
  goto grecs_grecs_returnlab;


/*-----------------------------------------------------------.
| grecs_grecs_exhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
grecs_grecs_exhaustedlab:
  grecs_grecs_error (YY_("memory exhausted"));
  grecs_grecs_result = 2;
  goto grecs_grecs_returnlab;


/*----------------------------------------------------------.
| grecs_grecs_returnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
grecs_grecs_returnlab:
  if (grecs_grecs_char != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      grecs_grecs_token = YYTRANSLATE (grecs_grecs_char);
      grecs_grecs_destruct ("Cleanup: discarding lookahead",
                  grecs_grecs_token, &grecs_grecs_lval, &grecs_grecs_lloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (grecs_grecs_len);
  YY_STACK_PRINT (grecs_grecs_ss, grecs_grecs_ssp);
  while (grecs_grecs_ssp != grecs_grecs_ss)
    {
      grecs_grecs_destruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*grecs_grecs_ssp), grecs_grecs_vsp, grecs_grecs_lsp);
      YYPOPSTACK (1);
    }
#ifndef grecs_grecs_overflow
  if (grecs_grecs_ss != grecs_grecs_ssa)
    YYSTACK_FREE (grecs_grecs_ss);
#endif
  if (grecs_grecs_msg != grecs_grecs_msgbuf)
    YYSTACK_FREE (grecs_grecs_msg);
  return grecs_grecs_result;
}

#line 242 "grecs-gram.y"


int grecs_grecs_grecs_error_suppress;
	
int
grecs_grecs_error(char const *s)
{
	if (!grecs_grecs_grecs_error_suppress)
		grecs_error(&grecs_grecs_lloc, 0, "%s", s);
	return 0;
}

struct grecs_node *
grecs_grecs_parser(const char *name, int traceflags)
{
	int rc;
	if (grecs_lex_begin(name, traceflags & GRECS_TRACE_LEX))
		return NULL;
	grecs_grecs_debug = traceflags & GRECS_TRACE_GRAM;
	parse_tree = NULL;
	rc = grecs_grecs_parse();
	if (grecs_error_count)
		rc = 1;
	grecs_lex_end(rc);
	if (rc) {
		grecs_tree_free(parse_tree);
		parse_tree = NULL;
	}
	return parse_tree;
}




    

